\documentclass[hyperref={unicode}, xcolor=dvipsnames, aspectratio=169, trans]{beamer} % dvipsnames gives more built-in colors
\graphicspath{ {images/} }
%\mode<presentation>
\usetheme[secheader]{Boadilla}
%\useoutertheme{miniframes} % Alternatively: miniframes, infolines, split
\useinnertheme{circles}

\definecolor{ctublue}{cmyk}{1,0.43,0,0}% CTU Blue (primary)
\usefonttheme[onlymath]{serif}
\usecolortheme[named=ctublue]{structure}
\setbeamertemplate{caption}[numbered]
\setbeamerfont{footnote}{size=\tiny}
\addtobeamertemplate{footnote}{\vspace{-6pt}\advance\hsize-0.5cm}{\vspace{6pt}}
\makeatletter
% Alternative A: footnote rule
\renewcommand*{\footnoterule}{\kern -3pt \hrule \@width 2in \kern 8.6pt}
% Alternative B: no footnote rule
% \renewcommand*{\footnoterule}{\kern 6pt}
\makeatother


\usepackage[utf8]{inputenc}
\usepackage[backend=biber, style=numeric, citestyle=numeric, url=false, doi=false]{biblatex}
\AtEveryCitekey{\clearfield{url}}
\AtEveryCitekey{\clearfield{pagetotal}}
\AtEveryCitekey{\clearfield{month}}
\AtEveryCitekey{\clearfield{publisher}}
\AtEveryCitekey{\clearfield{journal}}
\AtEveryCitekey{\clearfield{volume}}
\AtEveryCitekey{\clearfield{number}}
\AtEveryCitekey{\clearfield{pages}}
\AtEveryCitekey{\clearfield{issn}}
\AtEveryCitekey{\clearfield{doi}}
\AtEveryCitekey{\clearfield{copyright}}
\AtEveryCitekey{\clearfield{urldate}}
%\AtEveryCitekey{\clearfield{year}}
\AtEveryCitekey{\clearfield{primaryclass}}
\AtEveryCitekey{\clearfield{archiveprefix}}
\AtEveryCitekey{\clearfield{eprint}}
% \addbibresource{presentation.bib}
\addbibresource{../literature-jabref.bib}
\usepackage{subcaption}
\usepackage{physics}
\usepackage{siunitx}
\usepackage{bm}
\usepackage{acronym}
% \usepackage{authblk}
%\usepackage{fontspec}
% \usepackage{unicode-math}
% \setmainfont{TeX Gyre Bonum}
% \setmainfont{Iosevka Aile}
% \setmathfont[math-style=ISO,bold-style=ISO]{TeX Gyre Bonum Math}
% \setmathfont[math-style=ISO,bold-style=ISO]{Latin Modern Math}

\newcommand{\ee}{\mathrm{e}} %eulerovo číslo
\newcommand{\ii}{\mathrm{i}} %imaginární jednotka
\newcommand{\tg}{\mathrm{tg}} %imaginární jednotka
\newcommand{\arctg}{\mathrm{arctg}} %imaginární jednotk
\newcommand{\Var}{\mathrm{Var}} %variance
\newcommand{\Prb}{\mathrm{Prb}} %variance
\newcommand{\Laplace}{\mathop{}\!\mathbin\bigtriangleup}
\newcommand{\DAlambert}{\mathop{}\!\mathbin\Box}
\newcommand{\wec}[1]{\ensuremath{\begin{pmatrix}#1\end{pmatrix}}} %vektor
\newcommand{\Mod}[1]{\ (\mathrm{mod}\ #1)} %modulo
\newcommand{\Hw}[2]{\mathrm{d}_H (#1,\ #2)} %Hamming weight

\title[QCBM gradient and derivative-free comparison]{
  Comparison of gradient and derivative-free learning methods for quantum
  circuit Born machine
}
\date{
  \today
}
\author[V. Hudeček \& A. Gábris]{
  Vlastimil Hudeček\inst{1}
  \and
  Aurél Gábris\inst{1, 2}
}
% \author[1]{
%   Aurél Gábris, Ph.D.
% }
% \affil[1]{
%   Department of Physics, Czech Technical University in Prague: Faculty of
%   Nuclear Sciences and Physical Engineering
% }

\institute[FNSPE@CTU, Wigner inst.]{
  \inst{1} Department of Physics, Czech Technical University in Prague: Faculty
  of Nuclear Sciences and Physical Engineering
  \and
  \inst{2} Institute for Solid State Physics and Optics, HUN-REN Wigner Research Centre for Physics
}

\logo{
  \includegraphics[height=1cm]{ctu_logo_blue.pdf}
}

% ACRONYMS
\acrodef{PQC}{Parameterized Quantum Circuit}
\acrodef{VQC}{Variational Quantum Circuit}
\acrodef{QCBM}{Quantum Circuit Born Machine}
\acrodef{QML}{Quantum Machine Learning}
\acrodef{COBYLA}{Constrained Optimization BY Linear Approximation}
\acrodef{ADAM}{Adam optimizer}
\acrodef{CMA-ES}{Covariance matrix adaptation evolution strategy}
\acrodef{TV}{Total Variation}
\acrodef{MMD}{Maximum Mean Discrepancy}
\acrodef{RBM}{Restricted Boltzmann Machine}


% \makeatletter
% \renewcommand\@makefnmark{\hbox{\@textsuperscript{\usebeamercolor[fg]{footnote mark}\usebeamerfont*{footnote mark}[\@thefnmark]}}}
% \renewcommand\@makefntext[1]{{\usebeamercolor[fg]{footnote mark}\usebeamerfont*{footnote mark}[\@thefnmark]}\usebeamerfont*{footnote} #1}
% \makeatother


%
% BEGIN DOCUMENT
%

\begin{document}
%\usebackgroundtemplate{\includegraphics[width=\paperwidth]{ctu_background.pdf}}{
%  \begin{frame}
%    \titlepage
%  \end{frame}
%}
\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}
\subsection{Objectives of the project}

% \subsection{Quantum computing}
\begin{frame}
  \frametitle{Quantum generative machine learning and objectives of the work}
	\begin{itemize}[<+->]
  \item Quantum generative machine learning (\acs{QML}) and parameterized
    quantum circuits (\acs{PQC})
    \begin{itemize}
    \item Generating samples from probability distributions
    \item \acs{PQC} as a circuit for \acs{QML}
    \item \acs{QCBM} as a for of \acs{PQC} with classical analogy
    \end{itemize}
  \item Learning of \acs{PQC} through optimization of parameters
  \item Optimization methods: derivative and gradient-free
  \item Comparison of methods with \acs{COBYLA}, \acs{CMA-ES} and \acs{ADAM} optimizers
  \item Numerical simulations and verification on quantum computers
  \end{itemize}
\end{frame}

\subsection{Results of the project}

\begin{frame}
  \frametitle{Main results of the research}
  \begin{itemize}[<+->]
  \item {
      \emph{New finding:} Connection between the method of optimization of
      \acs{QCBM} parameters and the circuit depth and the number of circuit
      parameters.
    }
    \begin{itemize}
    \item {
        For a limited depth circuit the gradient-free optimization is more
        effective
      }
    \item {
        For complex cases with deeper circuits the derivative optimization
        has an advantage
      }
    \end{itemize}
  \item Effective implementation of the \acs{QCBM} for quantum computer with
    parallel execution of \acs{QCBM} circuits.
  \end{itemize}
\end{frame}

\section{Quantum Circuit Born Machine}
\subsection{Restricted Boltzmann Machine - base for \acs{QCBM}}

\begin{frame}
  \frametitle{Restricted Boltzmann Machine}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}[<+->]
      \item {
          Two layer classical neural network for generative deep learning\footnote[frame]{\fullcite{AntoineJacquier2022}},
          same as \acs{QCBM} serves as a generator for joint probability distribution
        }
      \item {
          Joint probability determined by the \emph{Boltzmann distribution function}
          \begin{equation*}
            \label{eq:rbm-joint-prob}
            P(\bm{v}, \bm{h}) = \frac{\exp{-E(\bm{v}, \bm{h})}}{Z}
          \end{equation*}
        }
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.9\textwidth]{RBM.pdf}
        \caption{
          Restricted Boltzman Machine as a two layer neural network with a
          visible (white)\\ and a hidden (black) layer nodes,
        }
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\subsection{What is a Quantum Circuit Born Machine}

\begin{frame}
  \frametitle{Quantum Circuit Born Machine}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{itemize}[<+->]
      \item {
          Using the Born rule as a mean of generating (probability) distribution
          instead of relying of Boltzman distribution\footnote[frame]{\fullcite{Kondratyev2021}}
          \begin{equation*}
            \label{eq:qcbm-born-rule}
            P(\bm{v}, \bm{h}) = \expval{\mathcal{P}_{\bm{v}, \bm{h}}^{\dag} \mathcal{P}_{\bm{v}, \bm{h}}}{\psi},
          \end{equation*}
          where $\mathcal{P}_{\bm{v}, \bm{h}}$ denotes measurement operator
          which fulfills $\sum_{\bm{v}, \bm{h}}\mathcal{P}_{\bm{v}, \bm{h}}
          = \mathbb{I}$
        }
      \item {
          For the case of \acs{PQC} with parameter vector $\bm{\theta}$ this can be rewritten as $P = \abs{\braket{\phi}{\psi_{\bm{\theta}}}}^2$
        }
      \end{itemize}
    \end{column}
    \begin{column}{0.6\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.9\textwidth]{qcbm.pdf}
        \caption{Schematic of a \acs{QCBM}}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\section{Bechmarking of \acs{QCBM}}

\subsection{Test distribution}

\begin{frame}
  \frametitle{Test distribution}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{itemize}[<+->]
      \item {
          Mixing of normal distributions
          \begin{equation*}
            \label{eq:test-distribution}
            P(x) = \frac{1}{N} \sum_{i=1}^{4} w_i \exp{-\frac{(x-p_i)^2}{2 \sigma_i^2}}
          \end{equation*}
        }
      \item {
          Table of parameters of mixed normal distributions
          \begin{center}
            \begin{tabular}[htb]{|c|c|c|c|}
              \hline
              $i$ & $p_i$ & $\sigma_i$ & $w_i$ \\
              \hline
              $1$ & $-3$  & $0.3$      & $0.4$ \\
              \hline
              $2$ & $-1$  & $0.6$      & $0.1$ \\
              \hline
              $3$ & $+1$  & $0.3$      & $0.3$ \\
              \hline
              $4$ & $+3$  & $0.6$      & $0.2$ \\
              \hline
            \end{tabular}
          \end{center}
        }
      \end{itemize}
    \end{column}
    \begin{column}{0.6\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{RP01-QCBM-E1-prob_data.pdf}
        \caption{Graph of the benchmark probability\\ distribution}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\subsection{Tested gradient-free and derivative optimization methods}

\begin{frame}
  \frametitle{Cost function for optimization algorithms}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \textbf{Total Variation (\acs{TV})}
      \end{center}
      \begin{itemize}[<+->]
      \item {
          Cost function formula
          \begin{equation*}
            \label{eq:total-variation}
            \begin{split}
              f_{\text{TV}}(\bm{\theta}) &= f_{\text{TV}}\left(P_{\text{model}, \bm{\theta}}(x), P_{\text{data}}(x)\right)\\
                                         &= \sum_x \Big( P_{\text{model}, \bm{\theta}}(x) - P_{\text{data}}(x) \Big)^2
            \end{split}
          \end{equation*}
        }
      \end{itemize}
      \vspace{2.3cm}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \textbf{Maximum Mean Discrepancy (\acs{MMD})}
      \end{center}
      \begin{itemize}[<+->]
      \item {
          Cost function formula\footnote[frame]{\fullcite{Liu2018}},
          \begin{equation*}
            \label{eq:maximum-mean-discrepancy}
            \begin{split}
              f_{\text{MMD}}(\bm{\theta})
                                          &\approxeq \mathbb{E} \left[ K(P_{\text{model}, \bm{\theta}}, P_{\text{model}, \bm{\theta}}) \right] \\
                                          &- 2 \mathbb{E} \left[ K(P_{\text{model}, \bm{\theta}}, P_{\text{data}}) \right] \\
                                          &\color{Gray}{+ \mathbb{E} \left[ K(P_{\text{data}}, P_{\text{data}}) \right]}
            \end{split}
          \end{equation*}
        }
      \item {
          Kernel function
          \begin{equation*}
            \label{eq:rbf-kernel}
            K(x, y) = \frac{1}{k} \sum_{i = 1}^k \exp{-\frac{1}{2 \sigma_i} \abs{x-y}^2}
          \end{equation*}
        }
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Tested optimization algorithms}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \textbf{Gradient-free optimization}
      \end{center}
      \begin{itemize}[<+->]
      \item {
          \acs{COBYLA} optimizer
          \begin{itemize}
          \item Low number of cost function evaluations
          \item Low noise resilience, higher number of shots
          \end{itemize}
        }
      \item {
          \acs{CMA-ES} optimizer
          \begin{itemize}
          \item Allows for batch calculations
          \item Able to converge fast
          \end{itemize}
        }
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \textbf{Derivative optimization}
      \end{center}
      \begin{itemize}[<+->]
      \item {
          \acs{ADAM} optimizer
          \begin{itemize}
          \item Usage of first derivative
          \item Batch calculation of gradient
          \item Analytic gradient for \acs{MMD} cost
          \item Momentum can increase noise resilience
          \end{itemize}
        }
      \end{itemize}
      \vspace{0.9cm}
    \end{column}
  \end{columns}
  \vspace{0.5cm}
  \hrule
  \vspace{0.5cm}
  \begin{itemize}[<+->]
  \item Finding the relation between circuit depth and the learning result
    for each of the optimization algorithms.
  \end{itemize}
\end{frame}

\section{Results of \acs{QCBM} bechmarking}

\subsection{\acs{COBYLA} optimizer - gradient-free}

\begin{frame}
  \frametitle{Optimal depth for the \acs{COBYLA} optimized \acs{QCBM}}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{cobyla-mumbai-depthFIX}
        \caption{The \acs{TV} cost reached for different depths of the \acs{QCBM}
          circuit with the \acs{COBYLA} optimizer}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{COBYLA-mumbai-distr}
        \caption{Probability distribution with lowest cost generated with the
          \acs{COBYLA} optimizer}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\subsection{\acs{CMA-ES} optimizer - gradient-free}

\begin{frame}
  \frametitle{Optimal depth for the \acs{CMA-ES} optimized \acs{QCBM}}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{CMAES-mumbai-depth.pdf}
        \caption{The\acs{TV} cost reached for different depths of the \acs{QCBM}
          circuit with \acs{CMA-ES} optimizer}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{CMAES-mumbai-distr.png}
        \caption{Probability distribution with lowest cost generated with the
          \acs{CMA-ES} optimizer}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\subsection{\acs{ADAM} optimizer - derivative}

\begin{frame}
  \frametitle{Optimal depth for the \acs{ADAM} optimized \acs{QCBM}}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{ADAM-mumbai-depth.pdf}
        \caption{The\acs{MMD} cost reached for different depths of the \acs{QCBM}
          circuit with \acs{ADAM} optimizer}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{ADAM-mumbai-distr.pdf}
        \caption{Probability distribution with lowest cost generated with the
          \acs{ADAM} optimizer}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Plot \acs{TV} and \acs{MMD} cost during learning}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{ADAM-mumbai-TVvsMMD-sim.pdf}
        \caption{Plot of \acs{TV} and \acs{MMD} cost function during learning on
        simulator}
      \end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{ADAM-mumbai-TVvsMMD-qc.png}
        \caption{Plot of \acs{TV} and \acs{MMD} cost function during learning on
        \texttt{ibmq\_mumbai}}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}


% \subsection{Progress of learning of \acs{QCBM}}
%
% \begin{frame}
%   % \frametitle{Optimal depth for the \acs{ADAM} optimized \acs{QCBM}}
%   \begin{table}[h]\centering
%     \begin{tabular}[htb]{|c|c|c|}
%       \movie[externalviewer, width=0.33\textwidth]{optional text}{videos/cob_pdfs_clean.mp4} &
%           \movie[width=0.33\textwidth]{optional text}{videos/cma_pdfs_clean.mp4} &
%               \movie[width=0.33\textwidth]{optional text}{videos/ada_pdfs_clean.mp4} \\
%       \movie[width=0.33\textwidth]{optional text}{videos/cob_cost_clean.mp4} &
%           \movie[width=0.33\textwidth]{optional text}{videos/cma_cost_clean.mp4} &
%               \movie[width=0.33\textwidth]{optional text}{videos/ada_cost_clean.mp4}
%     \end{tabular}
%     \caption{Progress of learning of \acs{QCBM} with \acs{COBYLA}, \acs{CMA-ES}
%       and \acs{ADAM} from left to right.}
%     \label{tab:videos}
%   \end{table}
% \end{frame}

% \begin{frame}
%   \frametitle{Studied quantum algorithms}
%   \begin{columns}
%     \begin{column}{0.5\textwidth}
%       \begin{itemize}
%       \item Noisy Intermediate Scale Quantum (NISQ) devices and algorithm choice
%       \item Problems of depth, number of qubits and noise resilience
%       \item Low level algorithms: QAOA, VQE
%       \end{itemize}
%       \begin{definition}
%         The term \textit{VQA} refers to hybrid quantum algorithms, sharing common structure. The problem is encoded into
%         a cost function. Solving is based on an optimization loop.
%       \end{definition}
%     \end{column}
%     \begin{column}{0.5\textwidth}
%       \begin{figure}
%         \centering
%         \includegraphics[width=0.7\textwidth]{vqa.pdf}
%         \caption{Structure of VQA}
%       \end{figure}
%     \end{column}
%   \end{columns}
% \end{frame}
%
% \section{Quantum approximate optimization algorithm}
%
% \begin{frame}
%   \frametitle{QAOA}
%   \begin{itemize}
%   \item Scope: Combinatorial optimization problems
%   \end{itemize}
%   \begin{definition}
%     An \textit{objective function} is a map representing an optimization problem.
%     \begin{equation*}
%       \label{eq:obj-fun}
%       S \longrightarrow \mathbb{R},
%     \end{equation*}
%     where S is the set of values of variables.
%   \end{definition}
%   \begin{itemize}
%   \item New problem: Maximization or minimization of the objective function
%   \end{itemize}
%   \begin{figure}
%         \centering
%         \includegraphics[width=0.7\textwidth]{qaoa_schematic}
%         \caption{Structure of QAOA circuit}
%   \end{figure}
% \end{frame}
%
% \begin{frame}
%   \frametitle{QAOA in depth}
%   \begin{itemize}
%   \item Phase Hamiltonian: $H_P\ket{z}:=C(z)\ket{z}$
%   \item Phase operator: $U_P(\gamma):=\exp(-\ii\gamma H_P)=\prod_{\alpha=1}^m\exp(-\ii\gamma C_\alpha)$
%   \item Mixing Hamiltonian: $H_M:=\sum_{j=1}^n\sigma_j^X$
%   \item Mixing operator: $U_M = \prod_{j=1} \big( R_x (2 \beta)\big)_j$
%   \item $p$-level QAOA: $\ket{\vec{\gamma},\vec{\beta}}:=U_M(\beta_p)U(\gamma_p)\dots U(\beta_1)U_p(\gamma_1)\ket{s}$
%   \item $p$ limit: $\lim_{p \to \infty}\max_{\vec{\gamma}\in\left[0,2\pi\right),\vec{\beta}\in\left[0,\pi\right)}\expval{H_p}(\vec{\gamma},\vec{\beta})=\max_{z \in [n]}C(z)$
%   \item Rules for deriving the phase Hamiltonian for Boolean objective functions:
%     \begin{align}
%       H_{\neg f}  &= I-H_f            & H_{f \implies g} &=I-H_f+H_fH_g \nonumber\\
%       H_{f \land g} &= H_fH_g          & H_{f \lor g}      &= H_f + H_g - H_fH_g \nonumber\\
%       H_{f \oplus g}&= H_f+H_g-2H_fH_g & H_{af+bg}       &=aH_f+bH_g \nonumber
%     \end{align}
%   \end{itemize}
% \end{frame}
%
% \section{Variational Quantum Eigensolver}
%
% \begin{frame}
%   \frametitle{VQE}
%   \begin{columns}
%     \begin{column}{0.5\textwidth}
%       \begin{definition}
%         Eigenvalue algorithms find or estimate one or more eigenvalues of an operator, i.e. $T(\mathbf{v}) = \lambda \mathbf{v}$.
%       \end{definition}
%       \begin{itemize}
%       \item Finding eigenvalues for Hamiltonians as the lowest upper bound for ground state energy
%       \end{itemize}
%       \begin{align}
%           \label{eq:grnd-en}
%           E_0 &\leq \flatfrac{\expval{H}{\psi(\bm{\theta})}}{\braket{\psi(\bm{\theta})}} \nonumber \\
%           E_{\text{VQE}} &= \min_\theta \expval{U^\dagger(\bm\theta) H U(\bm\theta)}{\mathbf{0}} \nonumber
%       \end{align}
%     \end{column}
%     \begin{column}{0.5\textwidth}
%       \vspace{-1cm}
%       \begin{figure}
%         \centering
%         \includegraphics[width=0.9\textwidth]{vqe_diagram}
%         \caption{Structure of VQE}
%       \end{figure}
%     \end{column}
%   \end{columns}
% \end{frame}
%
% \begin{frame}
%   \frametitle{VQE in depth}
%   \begin{itemize}
%   \item \textit{ab initio molecular} Hamiltonians approximated as $H = T_e + V_{ne} + V_{ee}$
%   \item First quantization: Antisymmetrization algorithm: $T:\quad\ket{r_1 \dots r_\eta} \longmapsto \sum_{\sigma \in S_\eta}(-1)^{\pi(\sigma)} \ket{\sigma(r_1,\dots,r_\eta)}$
%   \item Second quantization: Jordan-Wigner encoding: $ a_j^\dagger \rightarrow \dyad{1}{0}_j = \pmqty{0 & 0 \\ 1 & 0} =\frac{X_j - \ii Y_j}{2}, \quad
%     a_j \rightarrow \dyad{0}{1}_j = \pmqty{0 & 1 \\ 0 & 0} =\frac{X_j + \ii Y_j}{2}$
%   \item Measurement strategies: Weighting $S = N_p \sum_{a=1}^{N_p} \frac{w_a^2 \Var(P_a)}{\epsilon^2} \implies S_a \propto w_a \sqrt{\Var(P_a)}$
%   \item Ansatz, barren plateau problem, HEA and Unitary Couple Cluster:
%   \end{itemize}
%   \vspace{-0.1cm}
%   \begin{figure}
%      \centering
%      \begin{subfigure}[b]{0.32\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{ng1}
%          \caption{Without barren plateau}
%      \end{subfigure}
%      \begin{subfigure}[b]{0.32\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{ng2}
%          \caption{With barren plateau}
%      \end{subfigure}
% \end{figure}
%
%
% \end{frame}
%
% \section{Quantum programming tools}
%
% \begin{frame}
%   \frametitle{Tools for research of QC}
%   \begin{itemize}
%   \item Roles of software for quantum information research:
%     \begin{itemize}
%     \item High level programming languages
%     \item Low level programming languages
%     \item Quantum simulators
%     \item Quantum algorithms
%     \end{itemize}
%   \item Instruction sets:cQASM, openQASM
%   \item Languages: imperative: QCL, Q\#, Silq; functional: QML, Liqui$|>$, Quipper
%   \item SDK: Qiskit (IBM), QDK, (Microsoft), Cirq (Google), Ocean (D-Wave), Piquasso (BQCG)
%   \end{itemize}
% \end{frame}
%
% \begin{frame}
%   \frametitle{Qiskit}
%   \begin{itemize}
%   \item Built as an open source (MIT) Python library
%   \item Includes (noisy) simulators as well as models of real QC
%   \item Utilization of openQASM for communication with quantum hardware
%   \item IBM Quantum Experience
%   \end{itemize}
%   \begin{figure}
%     \centering
%     \includegraphics[width=0.9\textwidth]{qiskit-mpl}
%     \caption{Superdense coding circuit, created using \texttt{matplotlib}}
%   \end{figure}
% \end{frame}
%
% \section{Algorithm implementation and application}
% \subsection{Basic Implementations}
%
% \begin{frame}
%   \frametitle{Basic QAOA}
%   \begin{definition}
%     \textit{MaxCut}: find the bipartition of vertices of an edge-weighted graph that maximizes the weight of the edges crossing the partition.
%   \end{definition}
%   \begin{itemize}
%   \item MaxCut is NP complete problem with objective function: $C(x) = \sum_{\langle u,v \rangle}(x_u \oplus x_v)$
%   \item Phase Hamiltonian: $H_P=\sum_{\langle u,v \rangle}H_{p\langle u,v \rangle}=\sum_{\langle u,v \rangle}\frac{1}{2}(1-Z_uZ_v)$
%   \end{itemize}
%   \begin{figure}
%     \centering
%     \includegraphics[width=0.7\textwidth]{mc-def}
%     \caption{MaxCut for a simple graph}
%   \end{figure}
% \end{frame}
%
% \begin{frame}
%   \frametitle{Basic QAOA - implementation details}
%   \begin{figure}
%      \centering
%      \begin{subfigure}[b]{0.9\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{mc-circuit}
%          \caption{MaxCut circuit implementation}
%      \end{subfigure}
%      \\
%      \begin{subfigure}[b]{0.32\textwidth}
%        \centering
%        \includegraphics[width=\textwidth]{01-Maxcut-graph-0}
%        \caption{Simple MaxCut graph}
%      \end{subfigure}
%      \hfill
%      \begin{subfigure}[b]{0.32\textwidth}
%        \centering
%        \includegraphics[width=\textwidth]{01-Maxcut-graph-1}
%        \caption{Simple MaxCut graph - partitioned}
%      \end{subfigure}
%    \end{figure}
% \end{frame}
%
% \begin{frame}
%   \frametitle{Basic QAOA: results}
%   \vspace{-0.4cm}
%   \begin{figure}[h]
%   \centering
%   \includegraphics[width=0.45\textwidth]{01_Maxcut-histo-01}
%   \includegraphics[width=0.45\textwidth]{01_Maxcut-histo-02}
%   \\
%   \includegraphics[width=0.45\textwidth]{01_Maxcut-histo-03}
%   \includegraphics[width=0.45\textwidth]{01_Maxcut-histo-04}
%   \caption{QAOA:Simple GD, Scheduled GD, RMSProp, COBYLA}
% \end{figure}
% \end{frame}
%
% \begin{frame}
%   \frametitle{Basic VQE}
%   \begin{columns}
%     \begin{column}{0.5\textwidth}
%       \begin{itemize}
%       \item Finding eigenvalue for a given matrix
%       \item Following the steps studied in chapter 3: Pauli strings decomposition, circuit analysis, weighting of measurements
%       \item Ansatz HEA (Hardware efficient ansatz):
%       \end{itemize}
%       \begin{equation*}
%         M = \pmqty{0 & 0 & -0.5 \\ 0 & 0.75 & -0.5 \\ -0.5 & -0.5 & -0.25},\quad
%         \lambda_1 = -0.75, \quad \lambda_2 = 0.25, \quad \lambda_3 = 1
%       \end{equation*}
%     \end{column}
%     \begin{column}{0.5\textwidth}
%       \begin{figure}
%         \centering
%         \includegraphics[width=0.8\textwidth]{hea}
%         \caption{HEA circuit block}
%       \end{figure}
%     \end{column}
%   \end{columns}
% \end{frame}
%
% \begin{frame}
%   \frametitle{Basic VQE: results}
%   \vspace{-0.4cm}
%   \begin{figure}[h]
%     \centering
%     \includegraphics[width=0.45\textwidth]{images/RMSProp_results}
%     % \\
%     \includegraphics[width=0.45\textwidth]{RMSProp_nfev}
%     \\
%     \includegraphics[width=0.45\textwidth]{COBYLA_results}
%     % \\
%     \includegraphics[width=0.45\textwidth]{COBYLA_nfev}
%     \caption{L: approx. of eigenvalue (RMSProp, COBYLA). R: number of iterations}
%   \end{figure}
% \end{frame}
%
% \subsection{Practical implementations}
%
% \begin{frame}
%   \frametitle{Practical QAOA}
%   \begin{columns}
%     \begin{column}{0.5\textwidth}
%       \begin{definition}\label{def:clustering}
%         \textit{Clustering}: grouping the elements of a (training) set into groups, by similarity.
%       \end{definition}
%       \begin{itemize}
%       \item Phase Hamiltonian: $H_P=\sum_{\langle u,v \rangle}w_{\langle u,v \rangle}\frac{1}{2}(1-Z_uZ_v)$
%       \item Weight matrix
%       \item Two level QAOA
%       \end{itemize}
%     \end{column}
%     \begin{column}{0.5\textwidth}
%       \begin{figure}
%         \centering
%         \includegraphics[width=0.95\textwidth]{training_set}
%         \caption{Randomly generated dataset, that is to be grouped by bi-clustering through QAOA}
%       \end{figure}
%     \end{column}
%   \end{columns}
% \end{frame}
%
% \begin{frame}
%   \frametitle{Practical QAOA: results}
%   \vspace{-0.1cm}
%   \begin{figure}[h]
%     \centering
%     \includegraphics[width=0.45\textwidth]{COB_QAOA_results}
%     % \\
%     \includegraphics[width=0.45\textwidth]{COB_QAOA_nfev}
%     \\
%     \includegraphics[width=0.45\textwidth]{RMSProp_QAOA_results}
%     % \\
%     \includegraphics[width=0.45\textwidth]{RMSProp_QAOA_nfev}
%     %\caption{L:expectation value approx. (COBYLA, RMSProp). R: number of iterations}
%   \end{figure}
% \end{frame}

\section{Conclusion}
\subsection{Results and open questions}

\begin{frame}
  \frametitle{Conclusion}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}[<+->]
      \item {
          \emph{Numerical results} show the connection between the depth of
          \acs{PQC} and the optimal choice of optimizer for learning.
        }
      \item {
          Observed similarity to learning of classical \acs{RBM} in the
          connection depth and optimal learning method.
        }
      \item {
          Cost functions, e.g. \acs{MMD}, used in classical machine learning can
          be applied to quantum machine learning for performance advantage.
        }
      \item {
          Tendency of \acs{QCBM} to produce periodic distributions.
        }
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \textbf{Open questions:}
      \end{center}
      \begin{itemize}[<+->]
      \item {
          Scaling the learning for larger \acs{PQC} leads to \emph{sampling
            problem}.
        }
      \item Introduction of \emph{hints} for the learning of difficult distributions.
      \end{itemize}
      \vspace{0.9cm}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}%<1-| trans:0>
  \frametitle{Thank you for your attention}
  \begin{figure}
    \centering
    \includegraphics[width=0.3\textwidth]{qr-code-gitlab.pdf}
    \caption{QR code leading to the project repository}
  \end{figure}
  \begin{center}
    \href{
      https://campuscvut-my.sharepoint.com/:v:/g/personal/hudecvl1_cvut_cz/Eezo8h4QpQtGpYqVmtIuPyYBkdOxb8jeB-Dg5tEwo6Wxxg?nav=eyJyZWZlcnJhbEluZm8iOnsicmVmZXJyYWxBcHAiOiJPbmVEcml2ZUZvckJ1c2luZXNzIiwicmVmZXJyYWxBcHBQbGF0Zm9ybSI6IldlYiIsInJlZmVycmFsTW9kZSI6InZpZXciLCJyZWZlcnJhbFZpZXciOiJNeUZpbGVzTGlua0NvcHkifX0&e=o38fVY
    }{
      QCBM learning video
    }
  \end{center}
\end{frame}

\end{document}
