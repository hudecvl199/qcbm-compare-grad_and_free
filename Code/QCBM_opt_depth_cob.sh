#!/bin/bash
### Job Name
#PBS -N QCBM_opt_depth_cob

### Send email on abort or end
#PBS -m ae
#PBS -M hudecvl1@fjfi.cvut.cz

### required runtime 
#PBS -l walltime=23:59:00 
### queue for submission 
#PBS -q gpu 

### Merge output and error files 
#PBS -j oe 
 
### Request 64 GB of memory and 1 CPU core on 1 compute node 
#PBS -l select=1:mem=64G:ncpus=4:ngpus=1

cd $PBS_O_WORKDIR

source venv/bin/activate

python QCBM_opt_depth.py
