"""Tools for QCBM training with COBYLA."""
import numpy as np
from costfun_tools import (
    get_cost_function,
    get_gradient_analyticV1,
    get_gradient_analyticV1_torch
)
# from mmd_tools import mmd, rbf_kernel
# from scipy.optimize import minimize
from qiskit_algorithms.optimizers import ADAM
from qiskit_ibm_runtime import SamplerV2 as IBMSampler
from qiskit_ibm_runtime import (
    Batch,
    # Session,
    # Options,
    # QiskitRuntimeService
)
from multishot import readlayoutmap


def flatten_list(lst):
    """Flattens a list recursively."""
    flat_list = []
    for item in lst:
        if isinstance(item, list):
            flat_list.extend(flatten_list(item))
        else:
            flat_list.append(item)
    return flat_list


def train_adam(cfg: dict):
    """Train QCBM with ADAM on simulator."""
    if cfg.get("init_params") is None:  # init_params is None -> use zeros
        parameters = np.zeros(cfg.get("circuit").num_parameters)
    elif cfg.get("init_params") == 'rand':  # for 'rand' value generate random
        parameters = np.random.rand(cfg.get("circuit").num_parameters) * np.pi
    else:
        parameters = cfg.get("init_params")
    print("Initial parameters set to:")
    print(parameters)
    optimizer = ADAM(maxiter=cfg.get("max_iter"), amsgrad=True)
    # callback_dict = {
    #     "iters": 0,
    #     "par_hist" : []
    # }
    prob_history = []
    grad_history = []
    par_history = []
    if cfg.get("cost_variant") == "TV":
        raise ValueError("TV grad not implemented.")
    else:
        cost_fun = get_cost_function(cfg=cfg)
    multi = cfg.get("multi")
    if multi:
        layouts = readlayoutmap(
                    filename=cfg.get("layout_file"),
                    # circuits=config.get("circuits")
                )
        pm = cfg.get("pm")
        aux_gradient = {
            "multi": multi,
            "layouts": layouts,
            "pm": pm,
        }
    else:
        aux_gradient = None
    if cfg.get("cost_variant") == "MMD_torch":
        gradient = get_gradient_analyticV1_torch(
            p_data=cfg.get("prob_data"),
            sampler=cfg.get("sampler"),
            circuit=cfg.get("circuit"),
            maximmum=cfg.get("maximmum"),
            shots=cfg.get("shots"),
            history_prob=prob_history,
            history_grad=grad_history,
            history_par=par_history,
            progress=True,
            aux=aux_gradient
        )
    else:
        gradient = get_gradient_analyticV1(
            p_data=cfg.get("prob_data"),
            mmd_instance=cfg.get("mmd_instance"),
            sampler=cfg.get("sampler"),
            circuit=cfg.get("circuit"),
            maximmum=cfg.get("maximmum"),
            shots=cfg.get("shots"),
            history_prob=prob_history,
            history_grad=grad_history,
            history_par=par_history,
            progress=True,
            aux=aux_gradient
        )
    optimizer_result = optimizer.minimize(
        fun=cost_fun,
        x0=parameters,
        jac=gradient,
    )
    # Result dictionary, does not contain cost history
    # unless we use the numeric gradient
    res_dict = {
        "x": optimizer_result.x,  # Solution parameters, [float, ...]
        "fun": [optimizer_result.fun],  # Solution cost, float
        "nfev": [optimizer_result.nfev],  # Number of iterations, int
        "par_hist": par_history,
        "prob_history": prob_history,  # History of probabilities
        "grad_history": grad_history,
    }
    return res_dict


def train_adam_ibm(cfg: dict):
    """Train QCBM with ADAM."""
    if cfg.get("init_params") is None:  # init_params is None -> use zeros
        parameters = np.zeros(cfg.get("circuit").num_parameters)
    elif cfg.get("init_params") == 'rand':  # for 'rand' value generate random
        parameters = np.random.rand(cfg.get("circuit").num_parameters) * np.pi
    else:
        parameters = cfg.get("init_params")
    print("Initial parameters set to:")
    print(parameters)
    optimizer = ADAM(maxiter=cfg.get("max_iter"), amsgrad=True)
    # callback_dict = {
    #     "iters": 0,
    #     "par_hist" : []
    # }
    prob_history = []
    grad_history = []
    par_history = []
    # if cfg.get("cost_variant") != "MMD":
    #     raise ValueError("TV grad not implemented.")
    # else:
    #     cost_fun = get_cost_function(cfg=cfg)
    multi = cfg.get("multi")
    if multi:
        layouts = readlayoutmap(
                    filename=cfg.get("layout_file"),
                    # circuits=config.get("circuits")
                )
        pm = cfg.get("pm")
        aux_gradient = {
            "multi": multi,
            "layouts": layouts,
            "pm": pm,
        }
    else:
        aux_gradient = None
    if cfg.get("cost_variant") == "MMD_torch":
        with Batch(service=cfg.service, backend=cfg.backend) as batch:
            # COBYLA is not parallel => use Session instead of Batch.
            try:
                ibmsampler = IBMSampler(batch, options=cfg.options)
                cost_fun = get_cost_function(cfg=cfg)
                gradient = get_gradient_analyticV1_torch(
                    p_data=cfg.get("prob_data"),
                    sampler=ibmsampler,
                    num_qubits=cfg.get("num_qubits"),
                    circuit=cfg.get("circuit"),
                    shots=cfg.get("shots"),
                    history_prob=prob_history,
                    history_grad=grad_history,
                    history_par=par_history,
                    progress=True,
                    aux=aux_gradient,
                )
                optimizer_result = optimizer.minimize(
                    fun=cost_fun,
                    x0=parameters,
                    jac=gradient,
                )
            except:
                batch.cancel()
            batch.cancel()
    else:
        with Batch(service=cfg.service, backend=cfg.backend) as batch:
            # COBYLA is not parallel => use Session instead of Batch.
            try:
                ibmsampler = IBMSampler(batch, options=cfg.options)
                cost_fun = get_cost_function(cfg=cfg)
                gradient = get_gradient_analyticV1(
                    p_data=cfg.get("prob_data"),
                    mmd_instance=cfg.get("mmd_instance"),
                    sampler=ibmsampler,
                    num_qubits=cfg.get("num_qubits"),
                    circuit=cfg.get("circuit"),
                    shots=cfg.get("shots"),
                    history_prob=prob_history,
                    history_grad=grad_history,
                    history_par=par_history,
                    progress=True,
                    aux=aux_gradient,
                )
                optimizer_result = optimizer.minimize(
                    fun=cost_fun,
                    x0=parameters,
                    jac=gradient,
                )
            except:
                batch.cancel()
            batch.cancel()
    res_dict = {
        "x": optimizer_result.x,
        "fun": optimizer_result.fun,
        "nfev": optimizer_result.nfev,
        "par_hist": par_history,
        "prob_history": prob_history,
        "grad_history": grad_history,
    }
    return res_dict
