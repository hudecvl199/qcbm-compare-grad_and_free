"""Tools for QCBM training with COBYLA."""
import numpy as np
from costfun_tools import get_cost_tvV1, get_cost_mmdV1, get_cost_mmd_torchV1
from scipy.optimize import minimize
from qiskit_ibm_runtime import Sampler as IBMSampler
from qiskit_ibm_runtime import Session, Options, QiskitRuntimeService
import traceback

# TODO: Refactor train_cobyla to use cfg dict.


class cob_cfg:
    """Holder of cobyla experiment configuration."""

    def __init__(
            self,
            prob_data,
            circuit,
            max_iter,
            shots,
            maximmum,
            service=None,
            backend=None,
            sampler=None,
            options=None,
            init_params=None,
            rhobeg=1.0,
            cost_variant="TV",
            mmd_instance=None,
            aux=None
    ):
        """Init of Cobyla config class."""
        self.prob_data = prob_data
        self.circuit = circuit
        self.max_iter = max_iter
        self.maximmum = maximmum
        self.shots = shots
        self.service = service
        self.sampler = sampler
        self.options = options
        self.init_params = init_params
        self.rhobeg = rhobeg
        self.cost_variant = cost_variant
        self.mmd_instance = mmd_instance
        self.aux = aux
        self.backend = backend
        if self.service is None and self.sampler is None:
            raise TypeError("Either service or sampler need to be specified.")


def train_cobyla(cfg: cob_cfg):
    """Train QCBM with COBYLA on simulator."""
    callback_dict = {
        "iters": 0,
        "par_hist": []
    }
    cost_history = []

    def build_callback(callback_dict):
        """Build callback function for Scipy COBYLA."""
        def callback(current_vector):
            callback_dict["iters"] += 1
            callback_dict["par_hist"].append(current_vector)
        return callback

    callback = build_callback(callback_dict)

    if cfg.init_params is None:  # init_params is None -> use zeros
        parameters = np.zeros(cfg.circuit.num_parameters)
    elif cfg.init_params == 'rand':  # for 'rand' value generate random
        parameters = np.random.rand(cfg.circuit.num_parameters) * np.pi
    else:
        parameters = cfg.init_params

    if cfg.cost_variant == "TV":
        cost_fun = get_cost_tvV1(  # Get TV cost calculator
            p_data=cfg.prob_data,
            sampler=cfg.sampler,
            circuit=cfg.circuit.copy(),
            shots=cfg.shots,
            maximmum=cfg.maximmum,
            history=cost_history,
            aux=cfg.aux
        )
    elif cfg.cost_variant == "MMD":
        cost_fun = get_cost_mmdV1(  # Get TV cost calculator
            p_data=cfg.prob_data,
            mmd_instance=cfg.mmd_instance,
            sampler=cfg.sampler,
            circuit=cfg.circuit.copy(),
            shots=cfg.shots,
            maximmum=cfg.maximmum,
            history=cost_history,
            aux=cfg.aux
        )
    elif cfg.cost_variant == "MMD_torch":
        cost_fun = get_cost_mmd_torchV1(  # Get TV cost calculator
            p_data=cfg.prob_data,
            sampler=cfg.sampler,
            circuit=cfg.circuit.copy(),
            shots=cfg.shots,
            maximmum=cfg.maximmum,
            history=cost_history,
            aux=cfg.aux
        )
    else:
        raise ValueError("Invalid cost variant.")
    optimizer_result = minimize(
        fun=cost_fun,
        x0=parameters,
        method="cobyla",
        callback=callback,
        bounds=[(-np.pi, np.pi) for a in range(len(parameters))],
        tol=1e-8,
        options={'maxiter': cfg.max_iter, 'rhobeg': cfg.rhobeg}
    )
    # return optimizer_result, cost_history, callback_dict
    res_dict = {
        "x": optimizer_result.x,
        "fun": [optimizer_result.fun],
        "nfev": [optimizer_result.nfev],
        "iters": [callback_dict["iters"]],
        "par_hist": callback_dict["par_hist"],
        "cost_history": cost_history,
    }
    return res_dict


def train_cobyla_ibm(cfg: cob_cfg):
    """Train QCBM with COBYLA."""
    callback_dict = {
        "iters": 0,
        "par_hist": []
    }
    cost_history = []

    def build_callback(callback_dict):
        """Build callback function for Scipy COBYLA."""
        def callback(current_vector):
            callback_dict["iters"] += 1
            it = callback_dict["iters"]
            if it % 10 == 0:
                print(f"Iteration: {it}.")
            callback_dict["par_hist"].append(current_vector)
        return callback

    callback = build_callback(callback_dict)
    if cfg.init_params is None:
        parameters = np.zeros(cfg.circuit.num_parameters)
    elif cfg.init_params == 'rand':
        parameters = np.random.rand(cfg.circuit.num_parameters) * np.pi
    else:
        parameters = cfg.init_params

    print(f"service: {cfg.service}")
    print(f"backend: {cfg.backend}")
    with Session(cfg.service, backend=cfg.backend) as session:
        # COBYLA is not parallel => use Session instead of Batch.
        print(f"session: {session}")
        try:
            ibmsampler = IBMSampler(session=session, options=cfg.options)
            print(f"sampler: {ibmsampler}")
            if cfg.cost_variant == "TV":
                cost_fun = get_cost_tvV1(  # Get TV cost calculator
                    p_data=cfg.prob_data,
                    sampler=ibmsampler,
                    circuit=cfg.circuit.copy(),
                    shots=cfg.shots,
                    maximmum=cfg.maximmum,
                    history=cost_history,
                    aux=cfg.aux
                )
            elif cfg.cost_variant == "MMD":
                cost_fun = get_cost_mmdV1(  # Get TV cost calculator
                    p_data=cfg.prob_data,
                    mmd_instance=cfg.mmd_instance,
                    sampler=ibmsampler,
                    circuit=cfg.circuit.copy(),
                    shots=cfg.shots,
                    maximmum=cfg.maximmum,
                    history=cost_history,
                    aux=cfg.aux
                )
            elif cfg.cost_variant == "MMD_torch":
                cost_fun = get_cost_mmd_torchV1(  # Get TV cost calculator
                    p_data=cfg.prob_data,
                    sampler=ibmsampler,
                    circuit=cfg.circuit.copy(),
                    shots=cfg.shots,
                    maximmum=cfg.maximmum,
                    history=cost_history,
                    aux=cfg.aux
                )
            else:
                raise ValueError("Invalid cost variant.")
            # cost_fun = get_cost_tv(
            #     cfg.prob_data,
            #     ibmsampler,
            #     cfg.circuit.copy(),
            #     cfg.shots,
            #     cost_history)
            optimizer_result = minimize(
                fun=cost_fun,
                x0=parameters,
                method="cobyla",
                callback=callback,
                bounds=[(-np.pi, np.pi) for a in range(len(parameters))],
                tol=1e-8,
                options={'maxiter': cfg.max_iter, 'rhobeg': cfg.rhobeg}
            )
        except Exception as e:
            print(f"Failed: {e}")
            print(traceback.format_exc())
            session.cancel()
        session.cancel()
    # return optimizer_result, cost_history, callback_dict
    res_dict = {
        "x": optimizer_result.x,  # Solution parameters, [float, ...]
        "fun": [optimizer_result.fun],  # Solution cost, float
        "nfev": [optimizer_result.nfev],  # Number of calls to cost fun, int
        "iters": [callback_dict["iters"]],  # Number of iterations, int
        "par_hist": callback_dict["par_hist"],
        "cost_history": cost_history,
    }
    return res_dict
