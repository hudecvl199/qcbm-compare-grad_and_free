"""Run multiple shots simultaneously on quantum computer."""

import numpy as np
import tomlkit
import tomlkit.toml_file
from qiskit import QuantumCircuit
from qiskit.circuit import Parameter, ParameterVector
from qiskit.primitives import SamplerResult, PubResult, DataBin, BitArray
from qiskit.result import QuasiDistribution, ProbDistribution
from qiskit_ibm_runtime import SamplerV1, SamplerV2
from qiskit.transpiler import PassManager


def readlayoutmap(
        filename: str,
        circuits: list[QuantumCircuit] = None
) -> list[list[int]]:
    """
    Read layout for circuits from a TOML file.

    Example file:
    [backend]
    name = 'ibm-torino'
    num_qubits = 133

    [map]
    num = 6
    type = 'layout'

    [layout]
    l1 = [0, 1, 2, 3, 4]
    l2 = [5, 6, 7, 8, 9]
    """
    # load layout map from file
    lm = tomlkit.toml_file.TOMLFile(filename).read()
    # get backend name and number of qubits
    b = lm['backend']
    n = lm['map']['num']
    # get type of layout map
    t = lm['map']['type']
    # resolve type of layout map
    if t == 'layout':
        ls = [lm['layout'].get('l' + str(i)) for i in range(n)]
    elif t == 'part':
        raise NotImplementedError('part type is not implemented.')
    else:
        raise ValueError('Invalid type.')
    print(f"""Loaded layout map for backend {b.get('name')} of type {t}
    compatible with {b.get('compatible')}
    with following {n} {t}s:""")
    # print layout map
    for i, l in enumerate(ls):
        print(f"""[l{i}]:
        {l}""")
    # check number of qubits if circuit is given
    if circuits is None:
        return ls
    else:
        print("Checking number of qubits:")
        if t == 'layout':
            for i, qc in enumerate(circuits):
                assert qc.num_qubits == len(ls[i])
            print("Passed.")
            return ls[0:len(circuits)]
        else:
            raise NotImplementedError('part type is not implemented.')


def flatten_list(
        lists: list[list[int]]
) -> list[int]:
    """Flatten a list of lists to list."""
    layoutlist = []
    # for clone in clones:
    #     if len(layoutlist) == 0:
    #         m = 0
    #     else:
    #         m = np.max(layoutlist)
    #     tmp = [v + m for v in clone]
    #     layoutlist.extend(tmp)
    for l in lists:
        layoutlist.extend(l)
    return layoutlist


def join_circuit_parameters(
        circuits: list[QuantumCircuit],
        npar: int = None
) -> tuple[QuantumCircuit, ParameterVector]:
    """Join parameters of circuits."""
    if npar is None:
        npar = np.sum([qc.num_parameters for qc in circuits])
    v = ParameterVector('v', npar)
    j = 0
    vqcs = []
    # Join parameters of circuits
    for i, qc in enumerate(circuits):
        parmap = {}
        for k in range(qc.num_parameters):
            parmap.update({qc.parameters[k]: v[j]})
            j += 1
        vqcs.append(qc.assign_parameters(parmap))
    return vqcs, v


def join_circuits(
        circuits: list[QuantumCircuit],
        nqubits: int = None,
        multilayout: list[int] = None
) -> QuantumCircuit:
    """Join circuits into multicircuit according to multilayout."""
    if nqubits is None:
        nqubits = np.sum([qc.num_qubits for qc in circuits])
    multicirc = QuantumCircuit(nqubits, nqubits)
    k = 0
    for qc in circuits:
        nq = qc.num_qubits
        if multilayout is None:
            layout = list(range(k, k + nq))
        else:
            layout = multilayout[k:k + nq]
        print(f"layout: {layout}")
        multicirc.compose(qc, qubits=layout, clbits=layout, inplace=True)
        k += nq
    return multicirc


def multicircuit(
        circuits: list[QuantumCircuit],
        # multilayout: list[int]
) -> QuantumCircuit:
    """Get multicircuit from a list of circuits"""
    npar = np.sum([qc.num_parameters for qc in circuits])
    if npar > 0:
        vqcs, v = join_circuit_parameters(circuits=circuits, npar=npar)
    else:
        vqcs = circuits
    # return join_circuits(circuits=vqcs, multilayout=multilayout)
    return join_circuits(circuits=vqcs)


def multisamplerresult_qd(
        res: SamplerResult,
        circuits: list[QuantumCircuit],
        combine: bool
) -> list[QuasiDistribution]:
    """Get list of quasi distributions from a multi circuit."""
    bins_len = [qc.num_clbits for qc in circuits]
    # mq.num_clbits//nbins
    bins = [{} for _ in circuits]
    pd = res.quasi_dists[0].binary_probabilities(num_bits=np.sum(bins_len))
    for key, value in pd.items():
        j = 0
        for i in range(len(bins_len)):
            k = key[j:(j + bins_len[i])]
            v = bins[i].get(k)
            if v is None:
                bins[i].update({k: value})
            else:
                bins[i].update({k: value + v})
    list_qd = [
        QuasiDistribution(
            data=d,
            shots=res.metadata[0].get('shots'))
        for d in bins
    ]
    if combine:
        return [combine_quasidistributions(list_qd)]
    else:
        return list_qd


def combine_quasidistributions(
        qds: list[QuasiDistribution]
) -> QuasiDistribution:
    """Combine a list a quasi distributions into one quasi distribution."""
    combined = {}
    num_of_qdists = len(qds)
    for qd in qds:
        for key, value in qd.items():
            if combined.get(key) is None:
                combined.update({key: value})
            else:
                combined.update({key: combined.get(key) + value})
    for key, value in combined.items():
        combined.update({key: value/float(num_of_qdists)})
    return QuasiDistribution(data=combined, shots=qds[0].shots * num_of_qdists)


def multicount(
        counts: dict,
        circuits: list[QuantumCircuit],
        combine: bool
) -> list[dict]:
    """Get list of counts from multi circuit counts."""
    bins_len = [qc.num_clbits for qc in circuits]
    # mq.num_clbits//nbins
    bins = [{} for _ in circuits]
    for key, value in counts.items():
        j = 0
        for i in range(len(bins_len)):
            k = key[j:(j + bins_len[i])]
            v = bins[i].get(k)
            if v is None:
                bins[i].update({k: value})
            else:
                bins[i].update({k: value + v})
    if combine:
        return [combine_counts(bins)]
    else:
        return bins


def combine_counts(countslist: list[dict]) -> dict:
    """Combine a list of counts dictionaries into one."""
    combined = {}
    for counts in countslist:
        for key, value in counts.items():
            if combined.get(key) is None:
                combined.update({key: value})
            else:
                combined.update({key: combined.get(key) + value})
    return combined


def multisamplerresult(
        res: SamplerResult,
        circuits: list[QuantumCircuit],
        combine: bool
) -> SamplerResult:
    """Get sampler result from multi circuit SamplerResult."""
    qds = multisamplerresult_qd(
        res=res, circuits=circuits, combine=combine
    )
    return [
        SamplerResult(
            quasi_dists=[qd],
            metadata=res.metadata)
        for qd in qds
    ]


def multirunV1(
        sampler: SamplerV1,
        circuits: list[QuantumCircuit],
        list_parameter_values: list[list[float]],
        shots: int,
        pm: PassManager,
        combine: bool,
        # layouts: list[list[int]]
) -> list[SamplerResult]:
    """Run SamplerV1 in parallel for list of circuits."""
    if (
            list_parameter_values is not None
            and len(circuits) != len(list_parameter_values)
    ):
        raise ValueError(
            "List of circuits and parameters need to be of same length.")
    # if layouts is None:
    #     layouts = [qc.layout for qc in circuits]
    #     layout_is_not_set = any(layout is None for layout in layouts)
    #     if not layout_is_not_set:
    #         raise ValueError("Invalid circuit layout.")
    # else:
    #     multilayout = clones_to_layout(layouts)
    # TODO: Fix behavior of multilayout = None
    # multiqc = multicircuit(circuits=circuits, multilayout=multilayout)
    multiqc = multicircuit(circuits=circuits)
    if list_parameter_values is None:
        multijob = sampler.run(
            circuits=pm.run(multiqc),
            shots=shots)
    else:
        multipar = []
        for pars in list_parameter_values:
            multipar.extend(pars)
        # print(f"qc.num_qubits: {multiqc.num_qubits}")
        # print(f"qc.num_clbits: {multiqc.num_clbits}")
        # print(f"qc.num_parameters: {multiqc.num_parameters}")
        # print(f"pars: {multiqc.parameters}")
        # multiqc.draw()
        proc_multiqc = pm.run(multiqc)
        # print(f"proc_qc.num_qubits: {proc_multiqc.num_qubits}")
        # print(f"proc_qc.num_clbits: {proc_multiqc.num_clbits}")
        # print(f"proc_qc.num_parameters: {proc_multiqc.num_parameters}")
        # print(f"pars: {proc_multiqc.parameters}")
        # proc_multiqc.draw()
        multijob = sampler.run(
            circuits=proc_multiqc,
            parameter_values=multipar,
            shots=shots)
    return multisamplerresult(
        res=multijob.result(),
        circuits=circuits,
        combine=combine)


def multirunV2(
        sampler: SamplerV2,
        circuits: list[QuantumCircuit],
        list_parameter_values: list[list[float]],
        pm: PassManager,
        shots: int,
        combine: bool,
        # layouts: list[list[int]]
) -> list[PubResult]:
    """Run SamplerV2 in parallel for list of circuits."""
    if (
            list_parameter_values is not None
            and len(circuits) != len(list_parameter_values)
    ):
        raise ValueError(
            "List of circuits and parameters need to be of same length.")
    # if layouts is None:
    #     layouts = [qc.layout for qc in circuits]
    #     layout_is_not_set = any(layout is None for layout in layouts)
    #     if not layout_is_not_set:
    #         raise ValueError("Invalid circuit layout.")
    # else:
    #     multilayout = clones_to_layout(layouts)
    # TODO: Fix behavior of multilayout = None
    # multiqc = multicircuit(circuits=circuits, multilayout=multilayout)
    multiqc = multicircuit(circuits=circuits)
    if list_parameter_values is None:
        multiPUB = (
            pm.run(multiqc),
            None
        )
        multijob = sampler.run(pubs=[multiPUB], shots=shots)
    else:
        multipar = []
        for pars in list_parameter_values:
            multipar.extend(pars)
        multiPUB = (
            pm.run(multiqc),
            multipar
        )
        multijob = sampler.run(
            pubs=[multiPUB],
            shots=shots)
    # Get PUBResult from anything returned by sampler
    PUBres = multijob.result()[0]
    # Output is for example
    # dict_items([('c', BitArray(<shape=(), num_shots=1000, num_bits=12>))])
    # To get counts we do the following:
    PUBcounts = list(PUBres.data.items())[0][1].get_counts()
    multicounts = multicount(
        counts=PUBcounts,
        circuits=circuits,
        combine=combine)
    return [
        PubResult(
            DataBin(
                c=BitArray.from_counts(counts)
            )
        ) for counts in multicounts
    ]
