"""Tools for QCBM training with COBYLA."""
import numpy as np
from costfun_tools import get_cost_function
# from mmd_tools import mmd, rbf_kernel
# from scipy.optimize import minimize
from qiskit_ibm_runtime import SamplerV2 as IBMSampler
from qiskit_ibm_runtime import (
    Batch,
    # Session,
    # Options,
    # QiskitRuntimeService
)
from cmaes import CMA


def flatten_list(lst):
    """Flattens a list recursively."""
    flat_list = []
    for item in lst:
        if isinstance(item, list):
            flat_list.extend(flatten_list(item))
        else:
            flat_list.append(item)
    return flat_list


def train_cmaes(cfg: dict):
    """Train QCBM with CMA-ES on simulator."""
    if cfg.get("init_params") is None:  # init_params is None -> use zeros
        parameters = np.zeros(cfg.get("circuit").num_parameters)
    elif cfg.get("init_params") == 'rand':  # for 'rand' value generate random
        parameters = np.random.rand(cfg.get("circuit").num_parameters) * np.pi
    else:
        parameters = cfg.get("init_params")
    # CMA without warmstart
    cma_optimizer = CMA(
        mean=parameters,
        sigma=0.3,
        bounds=np.array([[-np.pi/2, np.pi/2] for _ in range(len(parameters))]))
    cost_history = []
    parameter_history = []
    num_generations = cfg.get("num_generations")
    cost_fun = get_cost_function(cfg=cfg)
    generation = 0
    while generation < num_generations:
        population = []
        pop_size = cma_optimizer.population_size
        print(f"Starting generation {generation} with popsize {pop_size}.")
        for _ in range(pop_size):
            x = cma_optimizer.ask()
            population.append(x)
            # parameter_history.append(x)
        parameter_history.append(population)
        # circuits = [cfg.get("circuit")] * len(population)
        # job = local_sampler.run(circuits=cma_circuits, parameter_values=x_list, shots = 4000)
        pop_cost = [cost_fun(x) for x in population]
        cost_history.append(pop_cost)
        cma_optimizer.tell(list(zip(population, pop_cost)))
        if cma_optimizer.should_stop():
            break
        generation += 1
    # return optimizer_result, cost_history, callback_dict
    fun = np.min(cost_history[-1])
    x = parameter_history[-1][np.argmin(cost_history[-1])]
    ch = [
        np.min(cost_history[g]) for g in range(generation)
    ]
    ph = [
        parameter_history[g][
            np.argmin(cost_history[g])
        ] for g in range(generation)
    ]
    res_dict = {
        "x": x,  # Solution parameters, [float, ...]
        "fun": [fun],  # Solution cost, best from last generation, float
        "nfev": [len(flatten_list(cost_history))],  # Number of iterations, int
        "iters": [generation],  # Number of actual generations, int
        "par_hist": ph,
        "cost_history": ch,
    }
    return res_dict


def train_cmaes_ibm(cfg: dict):
    """Train QCBM with CMAES."""
    if cfg.init_params is None:  # init_params is None -> use zeros
        parameters = np.zeros(cfg.get("circuit").num_parameters)
    elif cfg.init_params == 'rand':  # for 'rand' value generate random
        parameters = np.random.rand(cfg.get("circuit").num_parameters) * np.pi
    else:
        parameters = cfg.init_params
    # CMA without warmstart
    cma_optimizer = CMA(
        mean=parameters,
        sigma=0.3,
        bounds=np.array([[-np.pi/2, np.pi/2] for _ in range(len(parameters))]))
    cost_history = []
    parameter_history = []
    num_generations = cfg.get("num_generations")
    cost_fun = get_cost_function(cfg=cfg)
    generation = 0
    with Batch(service=cfg.service, backend=cfg.backend) as batch:
        # COBYLA is not parallel => use Session instead of Batch.
        try:
            ibmsampler = IBMSampler(batch, options=cfg.options)
            cfg.update({"sampler": ibmsampler})
            cost_fun = get_cost_function(cfg=cfg, cost_history=cost_history)
            while generation < num_generations:
                population = []
                pop_size = cma_optimizer.population_size
                for _ in range(pop_size):
                    x = cma_optimizer.ask()
                    population.append(x)
                    # parameter_history.append(x)
                parameter_history.append(population)
                # circuits = [cfg.get("circuit")] * len(population)
                # job = local_sampler.run(circuits=cma_circuits, parameter_values=x_list, shots = 4000)
                pop_cost = [cost_fun(x) for x in population]
                cost_history.append(pop_cost)
                cma_optimizer.tell(list(zip(population, pop_cost)))
                if cma_optimizer.should_stop():
                    break
                generation += 1
        except:
            batch.cancel()
        batch.cancel()
    # return optimizer_result, cost_history, callback_dict
    fun = np.min(cost_history[-1])
    x = parameter_history[-1][np.argmin(cost_history[-1])]
    ch = [
        np.min(cost_history[g]) for g in range(generation)
    ]
    ph = [
        parameter_history[g][
            np.argmin(cost_history[g])
        ] for g in range(generation)
    ]
    res_dict = {
        "x": x,  # Solution parameters, [float, ...]
        "fun": [fun],  # Solution cost, best from last generation, float
        "nfev": [len(flatten_list(cost_history))],  # Number of iterations, int
        "iters": [generation],  # Number of actual generations, int
        "par_hist": ph,
        "cost_history": ch,
    }
    return res_dict
