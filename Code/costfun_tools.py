"""Cost function tools module."""

import numpy as np
import torch
from mmd_tools import (
    # mmd,
    # rbf_kernel,
    torch_mmd
)
from process_data import (
    # dist,
    dist_pdf,
    counts_to_pd
)
from qiskit import QuantumCircuit
# from qiskit.primitives import Sampler
from qiskit_ibm_runtime import SamplerV1, SamplerV2
from qiskit.result import ProbDistribution
from multishot import (
    multirunV1,
    multirunV2,
    # readlayoutmap
)


def get_pdV1(
        circuit: QuantumCircuit,
        parameter_values: list[float, ...],
        sampler: SamplerV1,
        shots: int,
        aux: dict = None) -> ProbDistribution:
    """Use SamplerV1 to get ProbDistribution."""
    if aux is not None:
        multi = aux.get("multi")
        pm = aux.get("pm")
        # layouts = aux.get("layouts")
        if shots is None or shots == 0:
            multires = multirunV1(
                sampler=sampler,
                circuits=[circuit] * multi,
                list_parameter_values=[parameter_values] * multi,
                shots=None,
                pm=pm,
                combine=True,
                # layouts=layouts
            )
        else:
            multires = multirunV1(
                sampler=sampler,
                circuits=[circuit] * multi,
                list_parameter_values=[parameter_values] * multi,
                shots=shots // multi,
                pm=pm,
                combine=True,
                # layouts=layouts
            )
        pd = multires[0].quasi_dists[0].nearest_probability_distribution()
    else:
        if shots is None or shots == 0:
            pd = sampler.run(  # For use with Sampler()
                circuits=circuit,
                parameter_values=parameter_values
            ).result().quasi_dists[0].nearest_probability_distribution()
        else:
            pd = sampler.run(
                circuits=circuit,
                parameter_values=parameter_values,
                shots=shots
            ).result().quasi_dists[0].nearest_probability_distribution()
    return pd


def get_pdV2(
        circuit: QuantumCircuit,
        parameter_values: list[float, ...],
        sampler: SamplerV1,
        shots: int,
        aux: dict = None) -> ProbDistribution:
    """Use SamplerV2 to get ProbDistribution."""
    if aux is not None:
        multi = aux.get("multi")
        pm = aux.get("pm")
        # layouts = aux.get("layouts")
        if shots is None or shots == 0:
            multires = multirunV2(
                sampler=sampler,
                circuits=[circuit] * multi,
                list_parameter_values=[parameter_values] * multi,
                pm=pm,
                shots=None,
                combine=True,
                # layouts=layouts
            )
        else:
            multires = multirunV2(
                sampler=sampler,
                circuits=[circuit] * multi,
                list_parameter_values=[parameter_values] * multi,
                pm=pm,
                shots=shots // multi,
                combine=True,
                # layouts=layouts
            )
        pd = list(multires[0].data.items())[0][1].get_counts()
    else:
        pub = (circuit, parameter_values)
        if shots is None or shots == 0:
            # For use with statevector simulator
            job = sampler.run(pubs=[pub], shots=None)
        else:
            job = sampler.run(pubs=[pub], shots=shots)
            res = job.result()[0]  # PUBResult
            counts = list(res.data.items())[0][1].get_counts()
            pd = counts_to_pd(counts=counts)
    return pd


def get_cost_function(
        cfg: dict,
        cost_history: list = None):
    """Get cost function from config."""
    multi = cfg.get("multi")
    if multi:
        # layouts = readlayoutmap(
        #     filename=cfg.get("layout_file"),
        #     circuits=[cfg.get("circuit")] * multi
        # )
        pm = cfg.get("pm")
        aux = {
            "multi": True,
            # "layouts": layouts,
            "pm": pm
        }
    else:
        aux = None
    match cfg.get("cost_variant"):
        case "TV":
            cost_fun = get_cost_tvV1(  # Get TV cost calculator
                p_data=cfg.get("prob_data"),
                sampler=cfg.get("sampler"),
                circuit=cfg.get("circuit").copy(),
                shots=cfg.get("shots"),
                maximmum=cfg.get("maximmum"),
                history=cost_history,
                aux=aux
            )
        case "MMD":
            cost_fun = get_cost_mmdV1(
                p_data=cfg.get("prob_data"),
                mmd_instance=cfg.get("mmd_instance"),
                sampler=cfg.get("sampler"),
                circuit=cfg.get("circuit").copy(),
                shots=cfg.get("shots"),
                maximmum=cfg.get("maximmum"),
                history=cost_history,
                aux=aux)
        case "MMD_torch":
            cost_fun = get_cost_mmd_torchV1(
                p_data=cfg.get("prob_data"),
                sampler=cfg.get("sampler"),
                circuit=cfg.get("circuit").copy(),
                shots=cfg.get("shots"),
                maximmum=cfg.get("maximmum"),
                history=cost_history,
                aux=aux)
        case _:
            raise ValueError("Invalid cost variant.")
    return cost_fun


def get_cost_functionV2(
        cfg: dict,
        cost_history: list = None):
    """Get cost function from config."""
    multi = cfg.get("multi")
    if multi:
        # layouts = readlayoutmap(
        #     filename=cfg.get("layout_file"),
        #     circuits=[cfg.get("circuit")] * multi
        #     )
        pm = cfg.get("pm")
        aux = {
            "multi": True,
            # "layouts": layouts,
            "pm": pm
        }
    else:
        aux = None
    match cfg.get("cost_variant"):
        case "TV":
            cost_fun = get_cost_tvV2(  # Get TV cost calculator
                p_data=cfg.get("prob_data"),
                sampler=cfg.get("sampler"),
                circuit=cfg.get("circuit").copy(),
                shots=cfg.get("shots"),
                maximmum=cfg.get("maximmum"),
                history=cost_history,
                aux=aux
            )
        case "MMD":
            cost_fun = get_cost_mmdV2(
                p_data=cfg.get("prob_data"),
                mmd_instance=cfg.get("mmd_instance"),
                sampler=cfg.get("sampler"),
                circuit=cfg.get("circuit").copy(),
                shots=cfg.get("shots"),
                maximmum=cfg.get("maximmum"),
                history=cost_history,
                aux=aux)
        case "MMD_torch":
            cost_fun = get_cost_mmd_torchV2(
                p_data=cfg.get("prob_data"),
                sampler=cfg.get("sampler"),
                circuit=cfg.get("circuit").copy(),
                shots=cfg.get("shots"),
                maximmum=cfg.get("maximmum"),
                history=cost_history,
                aux=aux)
        case _:
            raise ValueError("Invalid cost variant.")
    return cost_fun


def get_cost_tvV1(
        p_data,
        sampler: SamplerV1,
        circuit: QuantumCircuit,
        shots: int,
        maximmum: int,
        history=None,
        aux=None):
    """Get TV cost function with bound p_data, circuit, shots and history."""

    def cost_tv_fun(parameters):
        """Return TV cost for given parameters."""
        # if shots is None or shots == 0:
        #     # For use with Sampler()
        #     prob = dist_pdf(
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum)
        # else:
        #     # For use with everything else
        #     prob = dist_pdf(
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters,
        #             shots=shots
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum
        #     )
        pd = get_pdV1(
            circuit=circuit,
            parameter_values=parameters,
            sampler=sampler,
            shots=shots,
            aux=aux)
        prob = dist_pdf(dist=pd, maximmum=maximmum)
        cost = np.sum(np.abs(prob-p_data)**2)
        if history is not None:
            history.append(cost)
        return cost
    return cost_tv_fun


def get_cost_tvV2(
        p_data,
        sampler: SamplerV2,
        circuit: QuantumCircuit,
        shots: int,
        maximmum: int,
        history=None,
        aux=None):
    """Get TV cost function with bound p_data, circuit, shots and history."""

    def cost_tv_funV2(parameters):
        """Return TV cost for given parameters."""
        # if shots is None or shots == 0:
        #     # For use with statevector simulator
        #     pub = (circuit, parameters)
        #     job = sampler.run(pubs=[pub], shots=None)
        #     res = job.result()[0]  # PUBResult
        #     counts = list(res.data.items())[0][1].get_counts()
        #     pd = counts_to_pd(counts=counts)
        #     prob = dist_pdf(dist=pd, maximmum=maximmum)
        # else:
        #     pub = (circuit, parameters)
        #     job = sampler.run(pubs=[pub], shots=shots)
        #     res = job.result()[0]  # PUBResult
        #     counts = list(res.data.items())[0][1].get_counts()
        #     pd = counts_to_pd(counts=counts)
        pd = get_pdV2(
            circuit=circuit,
            parameter_values=parameters,
            sampler=sampler,
            shots=shots,
            aux=aux)
        prob = dist_pdf(dist=pd, maximmum=maximmum)
        cost = np.sum(np.abs(prob-p_data)**2)
        if history is not None:
            history.append(cost)
        return cost
    return cost_tv_funV2


def get_cost_mmdV1(
        p_data,
        mmd_instance,
        sampler: SamplerV1,
        circuit: QuantumCircuit,
        maximmum: int,
        shots: int,
        history=None,
        aux=None):
    """Get MMD cost function with bound p_data, circuit, shots and history."""

    def cost_mmd_fun(parameters):
        """Return MMD cost for given parameters."""
        # maximmum = int(num_qubits * '1', 2) + 1
        # if shots is None or shots == 0:
        #     # For use with Sampler()
        #     prob = dist_pdf(
        #         # dist(sampler, circuit, parameters),
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum
        #     )
        # else:
        #     # For use with everything else
        #     prob = dist_pdf(
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters,
        #             shots=shots
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum
        #     )
        pd = get_pdV1(
            circuit=circuit,
            parameter_values=parameters,
            sampler=sampler,
            shots=shots,
            aux=aux)
        prob = dist_pdf(dist=pd, maximmum=maximmum)
        cost = mmd_instance.calc(prob, p_data)
        if history is not None:
            history.append(cost)
        return cost
    return cost_mmd_fun


def get_cost_mmdV2(
        p_data,
        mmd_instance,
        sampler: SamplerV2,
        circuit: QuantumCircuit,
        maximmum: int,
        shots: int,
        history=None,
        aux=None):
    """Get MMD cost function with bound p_data, circuit, shots and history."""

    def cost_mmd_funV2(parameters):
        """Return MMD cost for given parameters."""
        # maximmum = int(num_qubits * '1', 2) + 1
        # if shots is None or shots == 0:
        #     # For use with Sampler()
        #     prob = dist_pdf(
        #         # dist(sampler, circuit, parameters),
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum
        #     )
        # else:
        #     # For use with everything else
        #     prob = dist_pdf(
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters,
        #             shots=shots
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum
        #     )
        pd = get_pdV2(
            circuit=circuit,
            parameter_values=parameters,
            sampler=sampler,
            shots=shots,
            aux=aux)
        prob = dist_pdf(dist=pd, maximmum=maximmum)
        cost = mmd_instance.calc(prob, p_data)
        if history is not None:
            history.append(cost)
        return cost
    return cost_mmd_funV2


def get_cost_mmd_torchV1(
        p_data,
        sampler: SamplerV1,
        circuit: QuantumCircuit(),
        maximmum: int,
        shots: int,
        kernel='rbf',
        history=None,
        aux=None):
    """Get MMD cost function with bound p_data, circuit, shots and history."""
    device = torch.device(
        "cuda" if torch.cuda.is_available() else "cpu")

    def cost_mmd_fun(parameters):
        """Return MMD cost for given parameters."""
        # maximmum = int(num_qubits * '1', 2) + 1
        # if shots is None or shots == 0:
        #     # For use with Sampler()
        #     prob = dist_pdf(
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum
        #     )
        # else:
        #     # For use with everything else
        #     prob = dist_pdf(
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters,
        #             shots=shots
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum
        #     )
        pd = get_pdV1(
            circuit=circuit,
            parameter_values=parameters,
            sampler=sampler,
            shots=shots,
            aux=aux)
        prob = dist_pdf(dist=pd, maximmum=maximmum)
        cost = float(
            torch_mmd(
                x=torch.tensor(prob[np.newaxis]).cuda(),
                y=torch.tensor(p_data[np.newaxis]).cuda(),
                kernel=kernel,
                device=device)
        )
        if history is not None:
            history.append(cost)
        return cost
    return cost_mmd_fun


def get_cost_mmd_torchV2(
        p_data,
        sampler: SamplerV2,
        circuit: QuantumCircuit,
        maximmum: int,
        shots: int,
        kernel: str = 'rbf',
        history: list = None,
        aux=None
):
    """Get MMD cost function with bound p_data, circuit, shots and history."""
    device = torch.device(
        "cuda" if torch.cuda.is_available() else "cpu")

    def cost_mmd_fun(parameters):
        """Return MMD cost for given parameters."""
        # maximmum = int(num_qubits * '1', 2) + 1
        # if shots is None or shots == 0:
        #     # For use with Sampler()
        #     prob = dist_pdf(
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum
        #     )
        # else:
        #     # For use with everything else
        #     prob = dist_pdf(
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameters,
        #             shots=shots
        #         ).result().quasi_dists[0].nearest_probability_distribution(),
        #         maximmum
        #     )
        pd = get_pdV2(
            circuit=circuit,
            parameter_values=parameters,
            sampler=sampler,
            shots=shots,
            aux=aux
        )
        prob = dist_pdf(dist=pd, maximmum=maximmum)
        cost = float(
            torch_mmd(
                x=torch.tensor(prob[np.newaxis]).cuda(),
                y=torch.tensor(p_data[np.newaxis]).cuda(),
                kernel=kernel,
                device=device
            )
        )
        if history is not None:
            history.append(cost)
        return cost
    return cost_mmd_fun


def get_gradient_analyticV1(
        p_data,
        mmd_instance,
        sampler: SamplerV1,
        # num_qubits: int,
        maximmum: int,
        circuit: QuantumCircuit,
        shots: int,
        history_prob: list = None,
        history_grad: list = None,
        history_par: list = None,
        progress=False,
        aux=None
):
    """Get analytic gradient function with bound p_data, mmd, circuit, etc."""

    def gradient_analytic(parameters):
        """Analytic gradient function for parameter set."""
        # maximmum = int(num_qubits * '1', 2) + 1
        num_of_par = len(parameters)
        num_of_grad_points = 2 * num_of_par + 1
        # circuits_list = [circuit] * num_of_grad_points
        parameters_list = []
        gradient = []
        parameters_list.append(parameters.copy())
        for i in range(num_of_par):
            # pi/2 phase
            step = np.pi/2
            pars_plus = parameters.copy()
            pars_plus[i] += step
            parameters_list.append(pars_plus)
            # -pi/2 phase
            pars_minus = parameters.copy()
            pars_minus[i] -= step
            parameters_list.append(pars_minus)
        # # if shots is None:
        # #     # Noiseless
        # #     jobs = sampler.run(
        # #         circuits=circuits_list,
        # #         parameter_values=parameters_list
        # #     )
        # # else:
        # #     # Noisy simulation or quantum computer
        # #     jobs = sampler.run(
        # #         circuits=circuits_list,
        # #         parameter_values=parameters_list,
        # #         shots=shots
        # #     )
        # # results = jobs.result()
        # if shots is None or shots == 0:
        #     res = [
        #         sampler.run(
        #             circuits=circuit,
        #             parameter_values=parameter
        #         ).result()
        #         for parameter in parameters_list
        #     ]
        # else:
        #     res = []
        #     for parameter in parameters_list:
        #         res.append(
        #             sampler.run(
        #                 circuits=circuit,
        #                 parameter_values=parameter,
        #                 shots=shots
        #             ).result())
        # # pd_list = []
        # # for qd in results.quasi_dists:
        # #     pd_list.append(
        # #         dist_pdf(
        # #             qd.nearest_probability_distribution(),
        # #             maximmum
        # #         )
        # #     )
        pd_list = [
            dist_pdf(
                dist=get_pdV1(
                    circuit=c,
                    parameter_values=p,
                    sampler=sampler,
                    shots=shots,
                    aux=aux
                ),
                maximmum=maximmum
            )
            for c, p in zip(
                    [circuit] * len(parameters_list),
                    parameters_list)
        ]
        # for r in res:
        #     pd_list.append(dist_pdf(
        #         r.quasi_dists[0].nearest_probability_distribution(),
        #         maximmum))
        prob = pd_list[0]
        for i in range(1, num_of_grad_points, 2):
            prob_plus = pd_list[i]
            prob_minus = pd_list[i + 1]
            grad_plus = mmd_instance.kernel_expect(
                px=prob,
                py=prob_plus
            ) - mmd_instance.kernel_expect(
                px=prob,
                py=prob_minus
            )
            grad_minus = mmd_instance.kernel_expect(
                px=p_data,
                py=prob_plus
            ) - mmd_instance.kernel_expect(
                px=p_data,
                py=prob_minus
            )
            gradient.append(grad_plus - grad_minus)
        if history_prob is not None:
            history_prob.append(prob)
        if history_grad is not None:
            history_grad.append(gradient)
        if history_par is not None:
            history_par.append(parameters)
        if progress:
            print(f"||Gradient|| = {np.linalg.norm(gradient.cpu())}")
        return np.array(gradient)
    return gradient_analytic


def get_gradient_analyticV2(
        p_data,
        mmd_instance,
        sampler: SamplerV2,
        maximmum: int,
        circuit: QuantumCircuit,
        shots: int,
        history_prob: list = None,
        history_grad: list = None,
        history_par: list = None,
        progress=False,
        aux=None
):
    """Get analytic gradient function with bound p_data, mmd, circuit, etc."""

    def gradient_analytic(parameters):
        """Analytic gradient function for parameter set."""
        # maximmum = int(num_qubits * '1', 2) + 1
        num_of_par = len(parameters)
        num_of_grad_points = 2 * num_of_par + 1
        # circuits_list = [circuit] * num_of_grad_points
        parameters_list = []
        gradient = []
        parameters_list.append(parameters.copy())
        for i in range(num_of_par):
            # pi/2 phase
            step = np.pi/2
            pars_plus = parameters.copy()
            pars_plus[i] += step
            parameters_list.append(pars_plus)
            # -pi/2 phase
            pars_minus = parameters.copy()
            pars_minus[i] -= step
            parameters_list.append(pars_minus)
        pd_list = [
            dist_pdf(
                dist=get_pdV2(
                    circuit=c,
                    parameter_values=p,
                    sampler=sampler,
                    shots=shots,
                    aux=aux
                ),
                maximmum=maximmum)
            for c, p in zip(
                    [circuit] * len(parameters_list),
                    parameters_list)
        ]
        prob = pd_list[0]
        for i in range(1, num_of_grad_points, 2):
            prob_plus = pd_list[i]
            prob_minus = pd_list[i + 1]
            grad_plus = mmd_instance.kernel_expect(
                px=prob,
                py=prob_plus
            ) - mmd_instance.kernel_expect(
                px=prob,
                py=prob_minus
            )
            grad_minus = mmd_instance.kernel_expect(
                px=p_data,
                py=prob_plus
            ) - mmd_instance.kernel_expect(
                px=p_data,
                py=prob_minus
            )
            gradient.append(grad_plus - grad_minus)
        if history_prob is not None:
            history_prob.append(prob)
        if history_grad is not None:
            history_grad.append(gradient)
        if history_par is not None:
            history_par.append(parameters)
        if progress:
            print(f"||Gradient|| = {np.linalg.norm(gradient)}")
        return np.array(gradient)
    return gradient_analytic


def get_gradient_analyticV1_torch(
        p_data,
        sampler: SamplerV1,
        maximmum: int,
        circuit: QuantumCircuit,
        shots: int,
        kernel: str = 'rbf',
        history_prob: list = None,
        history_grad: list = None,
        history_par: list = None,
        progress=False,
        aux=None
):
    """Get analytic gradient function with bound p_data, mmd, circuit, etc."""
    device = torch.device(
        "cuda" if torch.cuda.is_available() else "cpu")

    def gradient_analytic(parameters):
        """Analytic gradient function for parameter set."""
        # maximmum = int(num_qubits * '1', 2) + 1
        num_of_par = len(parameters)
        num_of_grad_points = 2 * num_of_par + 1
        # circuits_list = [circuit] * num_of_grad_points
        parameters_list = []
        gradient = []
        parameters_list.append(parameters.copy())
        for i in range(num_of_par):
            # pi/2 phase
            step = np.pi/2
            pars_plus = parameters.copy()
            pars_plus[i] += step
            parameters_list.append(pars_plus)
            # -pi/2 phase
            pars_minus = parameters.copy()
            pars_minus[i] -= step
            parameters_list.append(pars_minus)
        pd_list = [
            dist_pdf(
                dist=get_pdV1(
                    circuit=c,
                    parameter_values=p,
                    sampler=sampler,
                    shots=shots,
                    aux=aux
                ),
                maximmum=maximmum)
            for c, p in zip(
                    [circuit] * len(parameters_list),
                    parameters_list)
        ]
        prob = pd_list[0]
        for i in range(1, num_of_grad_points, 2):
            prob_plus = pd_list[i]
            prob_minus = pd_list[i + 1]
            grad_plus = torch_mmd(
                x=torch.tensor(prob[np.newaxis]).cuda(),
                y=torch.tensor(prob_plus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device
            ) - torch_mmd(
                x=torch.tensor(prob[np.newaxis]).cuda(),
                y=torch.tensor(prob_minus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device
            )
            grad_minus = torch_mmd(
                x=torch.tensor(p_data[np.newaxis]).cuda(),
                y=torch.tensor(prob_plus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device
            ) - torch_mmd(
                x=torch.tensor(p_data[np.newaxis]).cuda(),
                y=torch.tensor(prob_minus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device
            )
            gradient.append(
                float(grad_plus - grad_minus)
            )
        if history_prob is not None:
            history_prob.append(prob)
        if history_grad is not None:
            history_grad.append(gradient)
        if history_par is not None:
            history_par.append(parameters)
        if progress:
            print(f"||Gradient|| = {np.linalg.norm(gradient)}")
        return np.array(gradient)
    return gradient_analytic


def get_gradient_analyticV2_torch(
        p_data,
        sampler: SamplerV2,
        maximmum: int,
        circuit: QuantumCircuit,
        shots: int,
        kernel: str = 'rbf',
        history_prob: list = None,
        history_grad: list = None,
        history_par: list = None,
        progress=False,
        aux=None
):
    """Get analytic gradient function with bound p_data, mmd, circuit, etc."""
    device = torch.device(
        "cuda" if torch.cuda.is_available() else "cpu")

    def gradient_analytic(parameters):
        """Analytic gradient function for parameter set."""
        # maximmum = int(num_qubits * '1', 2) + 1
        num_of_par = len(parameters)
        num_of_grad_points = 2 * num_of_par + 1
        # circuits_list = [circuit] * num_of_grad_points
        parameters_list = []
        gradient = []
        parameters_list.append(parameters.copy())
        for i in range(num_of_par):
            # pi/2 phase
            step = np.pi/2
            pars_plus = parameters.copy()
            pars_plus[i] += step
            parameters_list.append(pars_plus)
            # -pi/2 phase
            pars_minus = parameters.copy()
            pars_minus[i] -= step
            parameters_list.append(pars_minus)
        pd_list = [
            dist_pdf(
                dist=get_pdV2(
                    circuit=c,
                    parameter_values=p,
                    sampler=sampler,
                    shots=shots,
                    aux=aux
                ),
                maximmum=maximmum)
            for c, p in zip(
                    [circuit] * len(parameters_list),
                    parameters_list)
        ]
        prob = pd_list[0]
        for i in range(1, num_of_grad_points, 2):
            prob_plus = pd_list[i]
            prob_minus = pd_list[i + 1]
            grad_plus = torch_mmd(
                x=torch.tensor(prob[np.newaxis]).cuda(),
                y=torch.tensor(prob_plus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device
            ) - torch_mmd(
                x=torch.tensor(prob[np.newaxis]).cuda(),
                y=torch.tensor(prob_minus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device
            )
            grad_minus = torch_mmd(
                x=torch.tensor(p_data[np.newaxis]).cuda(),
                y=torch.tensor(prob_plus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device
            ) - torch_mmd(
                x=torch.tensor(p_data[np.newaxis]).cuda(),
                y=torch.tensor(prob_minus[np.newaxis]).cuda(),
                kernel=kernel,
                device=device
            )
            gradient.append(
                float(grad_plus - grad_minus)
            )
        if history_prob is not None:
            history_prob.append(prob)
        if history_grad is not None:
            history_grad.append(gradient)
        if history_par is not None:
            history_par.append(parameters)
        if progress:
            print(f"||Gradient|| = {np.linalg.norm(gradient)}")
        return np.array(gradient)
    return gradient_analytic
