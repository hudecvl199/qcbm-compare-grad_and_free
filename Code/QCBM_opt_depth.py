"""Python script for finding optimal depth of the QCBM circuit for COBYLA."""

# Imports section

from qiskit import (
    QuantumCircuit,
    # QuantumRegister
)
from qiskit.circuit.library import (
    # EfficientSU2,
    TwoLocal
)
# from qiskit.primitives import Sampler, BackendSampler
# from qiskit_aer import Aer, AerSimulator
from qiskit_aer.primitives import Sampler as AerSampler
from qiskit_aer.noise import NoiseModel
# from qiskit_algorithms.optimizers import ADAM, COBYLA
from qiskit_algorithms.utils import algorithm_globals
from qiskit_ibm_runtime import Sampler as IBMSampler
from qiskit_ibm_runtime import (
    Options,
    QiskitRuntimeService,
    # Session
)
from qiskit_ibm_runtime.options import (
    TranspilationOptions,
    # SimulatorOptions
)
from qiskit_ibm_runtime.fake_provider import FakeProviderForBackendV2
from qiskit.transpiler import PassManager
from qiskit.transpiler.preset_passmanagers import generate_preset_pass_manager
from qiskit_ibm_runtime.transpiler.passes.scheduling import (
    ALAPScheduleAnalysis,
    PadDynamicalDecoupling,
)
from qiskit.circuit.library import XGate
import numpy as np
# import pandas as pd
import time
import os
import shutil
# import configparser as cp
import tomlkit
import tomlkit.toml_file
# from scipy.stats import norm
# from scipy.optimize import minimize
from mmd_tools import mmd, rbf_kernel
# from process_data import dist, dist_pdf
# from costfun_tools import get_cost_function
from generate_prob_data import gen_prob_data
from cobyla_tools import cob_cfg, train_cobyla, train_cobyla_ibm
from cmaes_tools import train_cmaes, train_cmaes_ibm
from adam_tools import train_adam, train_adam_ibm
from multishot import readlayoutmap, clones_to_layout


cfgkeys = (
    "cfg_version",
    "variant",
    "use_ibm_q",
    "ibm_backend",
    "num_qubits",
    "shots",
    "fake_backend",
    "noise",
    "use_gpu",
    "depth_min",
    "depth_max",
    "rhobeg",
    "max_iter",
    "layout_import",
    "depths",
    "cost_variant",
    "num_generations",
    "multi",
    "layout_file"
)


def load_tomlconfig(
        filename: str,
        checkkeys: tuple[str] = None
) -> tomlkit.TOMLDocument:
    """Load TOML config file."""
    conftoml = tomlkit.toml_file.TOMLFile(filename).read()
    if checkkeys is not None and any(
            conftoml.get(key) is None for key in checkkeys
    ):
        raise ValueError("Missing config option.")
    # for key in checkkeys:
    #     if
    if conftoml.get("depths") == 0:
        if conftoml.get("depth_min") == 0 or conftoml.get("depth_max") == 0:
            raise ValueError("Depth must be specified.")
        else:
            conftoml["depths"] = list(range(
                conftoml.get("depth_min"),
                conftoml.get("depth_max")))
    else:
        if conftoml.get("depth_min") != 0 or conftoml.get("depth_max") != 0:
            raise ValueError("Depth cannot be ambiguous.")
    if conftoml.get("variant") not in ("cobyla", "cmaes", "adam"):
        raise ValueError("Invalid variant.")
    if conftoml.get("cost_variant") not in ("TV", "MMD", "MMD_torch"):
        raise ValueError("Invalid cost variant.")
    print("Loaded config:")
    print(conftoml)
    return conftoml


def create_datadir(
        datadirname: str = "data",
        archivename: str = "archive"
) -> str:
    """Create archive directory in data."""
    # Generated are automatically archived in data directory
    # In case ./data does not exist create it.
    if not os.path.isdir("data"):
        os.mkdir("data")
    # For each experiment we create a directory ./data/dirname
    dirname = (datadirname + "/" + archivename)
    # The dirname could already exist
    if os.path.isdir(dirname):
        copy_n = 1
        dirname_err = dirname + "X" + copy_n
        while os.path.isdir(dirname_err) and copy_n <= 99:
            copy_n += 1
            dirname_err = dirname + "X" + copy_n
        if copy_n == 99:
            raise ValueError("Too many same versions.")
        dirname = dirname_err
    os.mkdir(dirname)
    print(f"Created directory {dirname}.")
    return dirname


def get_passmanager(backend, layout):
    """Get pass manager for backand using layout."""
    target = backend.target
    pm = generate_preset_pass_manager(
        target=target,
        # optimization_level=3,
        optimization_level=1,
        layout_method='trivial',  # 'trivial' can be used
        initial_layout=layout)
    pm.scheduling = PassManager(
        [
            ALAPScheduleAnalysis(durations=target.durations()),
            PadDynamicalDecoupling(
                durations=target.durations(),
                dd_sequences=[XGate(), XGate()],
                pulse_alignment=target.pulse_alignment,
            ),
        ]
    )
    return pm


def get_dqc(cfg: tomlkit.TOMLDocument, depth: int, pm: PassManager = None):
    """Get QCBM quantum circuit  for depth."""
    nq = cfg.get("num_qubits")
    # print(f"nq = {nq}")
    # dqr = QuantumRegister(nq, nq)
    # dqc = QuantumCircuit(dqr)
    dqc = QuantumCircuit(nq, nq)
    dqc.h(dqc.qubits)
    # dansatz = EfficientSU2(
    #     nq,
    #     entanglement='circular',
    #     reps=depth
    # )
    dansatz = TwoLocal(
        num_qubits=nq,
        rotation_blocks='rz',
        entanglement_blocks='ecr',
        entanglement='circular',
        reps=depth,
    )
    dqc.compose(dansatz, inplace=True)
    dqc.measure(dqc.qubits, dqc.clbits)
    if pm is not None:
        dqc = pm.run(dqc)
    # dqc.measure_all()
    # if cfg.get("use_ibm_q") or cfg.get("noise"):
    #     # IBM backend or noisy simulator require transpilation
    #     dqc = pm.run(dqc)
    # dqc = pm.run(dqc)  # passmanager run should happen in later
    return dqc


def run_training(config: tomlkit.TOMLDocument, aux_config: dict) -> dict:
    """Run training for any valid variant."""
    match config.get("cost_variant"):
        case "MMD":
            if any(config.get(opt) is None
                   for opt in ("sigma_list", "basis", "kernel")):
                mmd_instance = mmd(
                    sigma_list=[0.4, 8],
                    basis=np.arange(
                        2**config.get("num_qubits")
                    ),
                    kernel=rbf_kernel
                )
            else:
                # TODO implement alternative kernels
                mmd_instance = mmd(
                    config.get("sigma_list"),
                    config.get("basis"),
                    config.get("kernel")
                )
        case _:
            mmd_instance = None
    match config.get("variant"):
        case "cobyla":
            multi = config.get("multi")
            if multi:
                layouts = aux_config.get("multilayout")
                pm = aux_config.get("passmanager")
                aux_cobyla = {
                    "multi": multi,
                    "layouts": layouts,
                    "pm": pm
                }
            else:
                aux_cobyla = None
            if config.get("use_ibm_q"):
                dcobyla_config = cob_cfg(
                    prob_data=aux_config.get("prob_data"),
                    circuit=aux_config.get("qc"),
                    max_iter=config.get("max_iter"),
                    shots=config.get("shots"),
                    rhobeg=config.get("rhobeg"),
                    cost_variant=config.get("cost_variant"),
                    service=aux_config.get("service"),
                    backend=aux_config.get("backend"),
                    maximmum=aux_config.get("maximmum"),
                    mmd_instance=mmd_instance,
                    aux=aux_cobyla
                )
                dres_dict = train_cobyla_ibm(dcobyla_config)
            else:
                dcobyla_config = cob_cfg(
                    prob_data=aux_config.get("prob_data"),
                    circuit=aux_config.get("qc"),
                    max_iter=config.get("max_iter"),
                    shots=config.get("shots"),
                    rhobeg=config.get("rhobeg"),
                    cost_variant=config.get("cost_variant"),
                    sampler=aux_config.get("sampler"),
                    backend=aux_config.get("backend"),
                    maximmum=aux_config.get("maximmum"),
                    mmd_instance=mmd_instance,
                    aux=aux_cobyla
                )
                dres_dict = train_cobyla(dcobyla_config)
        case "cmaes":
            cma_cfg = {
                # TOML part
                "max_iter": config.get("max_iter"),
                "shots": config.get("shots"),
                "rhobeg": config.get("rhobeg"),
                "cost_variant": config.get("cost_variant"),
                "num_generations": config.get("num_generations"),
                "multi": config.get("multi"),
                "layout_file": config.get("layout_file"),
                # AUX part
                "prob_data": aux_config.get("prob_data"),
                "circuit": aux_config.get("qc"),
                "backend": aux_config.get("backend"),
                "maximmum": aux_config.get("maximmum"),
                "pm": aux_config.get("passmanager")
            }
            match config.get("cost_variant"):
                case "MMD":
                    cma_cfg.update({"mmd_instance": mmd_instance})
                case _:
                    pass
            if config.get("use_ibm_q"):
                cma_cfg.update({"service": aux_config.get("service")})
                dres_dict = train_cmaes_ibm(cfg=cma_cfg)
            else:
                cma_cfg.update({"sampler": aux_config.get("sampler")})
                dres_dict = train_cmaes(cfg=cma_cfg)
        case "adam":
            adam_cfg = {
                # TOML part
                "max_iter": config.get("max_iter"),
                "shots": config.get("shots"),
                "rhobeg": config.get("rhobeg"),
                "cost_variant": config.get("cost_variant"),
                "num_generations": config.get("num_generations"),
                "num_qubits": config.get("num_qubits"),
                "multi": config.get("multi"),
                "layout_file": config.get("layout_file"),
                # AUX part
                "prob_data": aux_config.get("prob_data"),
                "circuit": aux_config.get("qc"),
                "backend": aux_config.get("backend"),
                "maximmum": aux_config.get("maximmum"),
                "pm": aux_config.get("passmanager")
            }
            match config.get("cost_variant"):
                case "MMD":
                    adam_cfg.update({"mmd_instance": mmd_instance})
                case "MMD_torch":
                    pass
                case _:
                    raise ValueError("TV numerical gradient not implemented.")
            if config.get("use_ibm_q"):
                adam_cfg.update({"service": aux_config.get("service")})
                dres_dict = train_adam_ibm(cfg=adam_cfg)
            else:
                adam_cfg.update({"sampler": aux_config.get("sampler")})
                dres_dict = train_adam(cfg=adam_cfg)
        case _:
            raise ValueError("Invalid algorithms variant.")
    return dres_dict


def loop_runner(config: tomlkit.TOMLDocument, aux_config: dict):
    """Run training for depths and save data."""
    for depth in config.get("depths"):
        variant = config.get("variant")
        print(f"Running experiment variant {variant}.")
        print(f"Depth set to {depth}.")
        if config.get("multi"):
            aux_config.update({"qc": get_dqc(
                cfg=config,
                depth=depth,
                # passmanager run needs to happen later
                # pm=aux_config.get("passmanager")
            )
                               })
        else:
            aux_config.update({"qc": get_dqc(
                cfg=config,
                depth=depth,
                # passmanager run needs to happen later
                pm=aux_config.get("passmanager")
            )
                               })
        dres_dict = run_training(
            config=config,
            aux_config=aux_config
        )
        savedata(config.get("variant") + "_D" + str(depth),
                 dirname=aux_config.get("dirname"),
                 config=config,
                 res_dict=dres_dict)


def savedata(expname: str,
             dirname: str,
             config:  tomlkit.TOMLDocument,
             res_dict: dict):
    """Save experiment results in numpy text files."""
    save_prefix = (dirname +
                   "/" +
                   word_join((
                       config.get("variant"),
                       "Ver",
                       config.get("cfg_version"),
                       expname,
                       time.strftime("%Y%m%d-%H%M%S")
                   ))
                   )
    for key, value in res_dict.items():
        print(key)
        print(value)
        np.savetxt(save_prefix + '-' + key + '.txt', value)


def word_join(t: tuple, separator: str = "-") -> str:
    """Safely join tuple which can be casted to string."""
    return separator.join(
        (str(s) for s in t)
    )


def main():
    """Run main function of the COBYLA training script."""
    # -----------------
    # | Setup section |
    # -----------------
    #
    conffilename = "QCBM_opt_depth.toml"
    cfg = load_tomlconfig(conffilename, cfgkeys)

    num_discrete_values = 2**cfg.get("num_qubits")
    maximmum = int(cfg.get("num_qubits") * '1', 2) + 1
    # variant_prefix = cfg.get("variant")[0:3].capitalize()
    dirname = create_datadir(
        datadirname="data",
        archivename=word_join((
            cfg.get("variant"),
            "Ver",
            cfg.get("cfg_version"),
            time.strftime("%Y%m%d-%H%M%S"),
        ))
    )
    # We want to have the probability data saved
    file_prefix = (dirname +
                   "/" +
                   word_join((
                       cfg.get("variant"),
                       "Ver",
                       cfg.get("cfg_version")
                   ))
                   )
    prob_data_filename = word_join((
        file_prefix,
        "prob_data.txt"
    ))
    # Prepare probability distribution data
    # prob_data is saved by gen_prob_data
    prob_data = gen_prob_data(num_discrete_values=num_discrete_values,
                              filename=prob_data_filename)
    # Save cobyla_cfg.toml and prob generator
    for f in (
            "QCBM_opt_depth.toml",
            "generate_prob_data.py",
            "QCBM_opt_depth.sh"
    ):
        try:
            shutil.copy(f, word_join((file_prefix, f)))
        except FileNotFoundError:
            print(f"WARNING: File {f} not found, no backup make.")
    # shutil.copy("cobyla_cfg.ini",
    #             "-".join((file_prefix, "cobyla_cfg.toml")))
    # shutil.copy("generate_prob_data.py",
    #             "-".join((file_prefix, "generate_prob_data.py")))
    # shutil.copy("QCBM_opt_depth_cob.sh",
    #             "-".join(file_prefix, "QCBM_opt_depth_cob.sh"))
    # Reference QCBM circuit
    # qr = QuantumRegister(cfg.get("num_qubits"))
    # qc = QuantumCircuit(qr)
    # qc.h(qc.qubits)
    # ansatz = EfficientSU2(
    #     num_qubits,
    #     entanglement='circular',
    #     reps=2)
    # qc.compose(ansatz, inplace=True)
    # qc.measure_all()
    # layout = Layout.from_intlist(cfg.get("layout_import"), qr)
    # MMD = mmd(
    #    sigma_list=[0.4, 8],
    #    basis=np.arange(2**num_qubits),
    #    kernel=rbf_kernel)

    # JUNCTION run simulation or quantum computer
    aux = {
        "prob_data": prob_data,
        "maximmum": maximmum,
        "dirname": dirname,
    }
    if cfg.get("use_ibm_q"):
        account = cfg.get("account")
        if account is None or account == 0:  # Use default account
            aux.update({"service": QiskitRuntimeService()})
        else:
            aux.update({"service": QiskitRuntimeService(name=account)})
        aux.update({"backend":
                    aux.get("service").get_backend(cfg.get("ibm_backend"))})
        # service = QiskitRuntimeService()
        # backend = service.get_backend(cfg.get("ibm_backend"))
        options = Options(
            # optimization_level=3,
            transpilation=TranspilationOptions(
                skip_transpilation=True
            )
        )
        # pm = get_passmanager(backend=backendx.get("backend"), layout=cfg.get("layout_import"))
    else:
        # backend = FakeProviderForBackendV2().backend(
        #     name=cfg.get("fake_backend")
        # )
        account = cfg.get("account")
        if account is None or account == 0:
            aux.update(
                {
                    "backend":
                    FakeProviderForBackendV2().backend(
                        name=cfg.get("fake_backend")
                    )
                 }
            )
        else:
            service = QiskitRuntimeService(name=account)
            aux.update(
                {
                    "backend": service.get_backend(cfg.get("ibm_backend"))
                }
            )
        if cfg.get("noise"):
            noise_model = NoiseModel.from_backend(aux.get("backend"))
            aer_backend_options = {'noise_model': noise_model,
                                   "seed_simulator":
                                   algorithm_globals.random_seed,
                                   "method": 'density_matrix',
                                   }
            # options = Options(
            #     transpilation=TranspilationOptions(skip_transpilation=True),
            #     simulator=SimulatorOptions(
            #         noise_model=noise_model
            #     )
            # )
            if cfg.get("use_gpu"):
                opts = ({"device": "GPU"}, {"batched_shots_gpu": True})
            else:
                opts = ({"device": "CPU"},)
            for opt in opts:
                aer_backend_options.update(opt)
            aux.update(
                {
                    "sampler": AerSampler(
                        backend_options=aer_backend_options,
                        skip_transpilation=True)
                }
            )
        else:
            aux.update(
                {
                    "backend":
                        FakeProviderForBackendV2().backend(
                            name=cfg.get("fake_backend")
                        )
                }
            )
            options = Options(
                transpilation=TranspilationOptions(
                    skip_transpilation=True
                )
            )
            aux.update(
                {
                    "sampler": IBMSampler(
                        backend=aux.get("backend"), options=options)
                }
            )
    multi = cfg.get("multi")
    if multi:
        multilayout = clones_to_layout(
            readlayoutmap(
                filename=cfg.get("layout_file"),
            )[0:multi]
        )
        print(f"multilayout: {multilayout}")
        aux.update(
            {
                "passmanager": get_passmanager(
                    backend=aux.get("backend"),
                    layout=multilayout
                ),
                "multilayout": multilayout
            }
        )
    else:
        aux.update(
            {
                "passmanager": get_passmanager(
                    backend=aux.get("backend"),
                    layout=cfg.get("layout_import")
                )
            }
        )
    loop_runner(config=cfg, aux_config=aux)


if __name__ == "__main__":
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
