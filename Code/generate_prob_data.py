"""Generate probability data."""
import numpy as np
import scipy


def gen_prob_data(num_discrete_values, filename=None):
    """Generate probability data."""
    coords = np.linspace(-4, 4, num_discrete_values)
    # Simple distribution
    # prob_data = normal.pdf(coords)
    prob_data = 0.4 * scipy.stats.norm(loc=-3, scale=0.3).pdf(coords)\
        + 0.3 * scipy.stats.norm(loc=1, scale=0.3).pdf(coords)\
        + 0.2 * scipy.stats.norm(loc=3, scale=0.6).pdf(coords)\
        + 0.1 * scipy.stats.norm(loc=-1, scale=0.6).pdf(coords)
    # Normalize
    prob_data = prob_data / np.sum(prob_data)
    if filename is not None:
        np.savetxt(filename, prob_data)
    return prob_data
