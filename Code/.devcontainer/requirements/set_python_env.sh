#!/usr/bin/env bash

PYTHON_ENV=$1

# For python
# python3 -m venv /opt/$PYTHON_ENV  \
#         && export PATH=/opt/$PYTHON_ENV/bin:$PATH \
#         && echo "source /opt/$PYTHON_ENV/bin/activate" >> ~/.bashrc

# source /opt/$PYTHON_ENV/bin/activate

# For conda
conda create --name ${PYTHON_ENV} --clone cuquantum-24.03
echo "conda activate ${PYTHON_ENV}" >> ~/.bashrc
conda activate ${PYTHON_ENV}

pip3 install -r ./requirements/requirements.txt